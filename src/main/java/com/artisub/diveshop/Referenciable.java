package com.artisub.diveshop;

public interface Referenciable<T> {
	
	void setReference(T t);
}
