package com.artisub.diveshop.view.pos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.persistence.NoResultException;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.ProductoDao;
import com.artisub.diveshop.controller.dao.VentaDao;
import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Producto;
import com.artisub.diveshop.model.Venta;
import com.artisub.diveshop.view.MainFrame;
import com.artisub.diveshop.view.catalogos.EditListaVentaItemDialog;



public class PuntoDeVenta extends JInternalFrame {
	
	IDAO<Producto> productoService = new ProductoDao();
	IDAO<Venta> ventaService = new VentaDao();
	protected List<Venta> lista_ventas = new ArrayList<Venta>();
	Venta venta = null;
	Producto producto = null;
	PuntoDeVenta self = this;
	
	Integer existencias;
	
	DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	
	private static final long serialVersionUID = -7182516204320202906L;
	public JPanel panel,contentPane,northPanel,northLeftPanel,northRightPanel,midPanel,sumatoriaPanel,labelPanel;
	public JTable table;
	private JScrollPane scrollPane;
	protected JLabel lblParte,lblModelo,lblCantidad,
					lblPrecioUnitario,lblExistencias, lblPrecioUusd,lblFecha,lblVenta,lblHora,lblSubTotalT,
					lblDescuentoT,lblPagadoT,lblCambioT,lblTotalT;
	private JLabel lblParte_1;
	public JLabel  totalLbl,subTotalLbl,descuentoLbl;
	
	public JButton btnAgregar,cobrarBtn,btnNuevaVenta,cancelarVentaBtn, otroBtn;
	private JRadioButton rdbtnTicket,rdbtnFactura;
	
	public DefaultTableModel model = new DefaultTableModel();
	protected JTextField numParteTextField, modeloTextField, cantidadTextField,precioTextField;
	protected JTextField fechaTextField, descuentoTextField,existenciasTextField,ventaNumTextField, precioPesosTextField,codigoTextField;

	public DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");
	
	//ACUMULADORES
	public Double subTotalAC=0.0, descuento=0.0, pagado=0.0, cambio=0.0, totalAC=0.0;
	private JPanel actionButtonPanel;
	private JButton borrarBtn;
	private JButton editarBtn;
	
	static Integer windowNumber = 1;
	
	/**
	 * Create the frame.
	 */
	public PuntoDeVenta() {
		super("Punto de Venta "+windowNumber.toString(), false, true, false,true);
		windowNumber++;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(115, 0, 1120,668);
		
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.scrollbar);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(15, 15));
		
		northPanel = new JPanel();
		northPanel.setBackground(SystemColor.scrollbar);
		contentPane.add(northPanel, BorderLayout.NORTH);
		
		northLeftPanel = new JPanel();
		northLeftPanel.setBackground(SystemColor.control);
		northLeftPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		northLeftPanel.setPreferredSize(new Dimension(630, 150));
		northPanel.add(northLeftPanel);
		northLeftPanel.setLayout(null);
		
		lblParte = new JLabel("Codigo:");
		lblParte.setForeground(Color.DARK_GRAY);
		lblParte.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblParte.setHorizontalAlignment(SwingConstants.RIGHT);
		lblParte.setBounds(10, 17, 63, 22);
		northLeftPanel.add(lblParte);
		
		
		numParteTextField = new JTextField();
		numParteTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		numParteTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		numParteTextField.setBounds(83, 55, 109, 26);
		numParteTextField.setColumns(10);
		numParteTextField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					numParteTextFieldSearch();            // realiza la busqueda al presionar la tecla Enter
				}
				
			}
		});
		northLeftPanel.add(numParteTextField);
		
		
		lblModelo = new JLabel("Descripcion:");
		lblModelo.setForeground(Color.DARK_GRAY);
		lblModelo.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblModelo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblModelo.setBounds(187, 17, 89, 14);
		northLeftPanel.add(lblModelo);
		
		modeloTextField = new JTextField();
		modeloTextField.setFont(new Font("Lao UI", Font.BOLD, 13));
		modeloTextField.setBounds(281, 14, 339, 26);
		modeloTextField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					openSearchFrame();
				}
			}
		});
		northLeftPanel.add(modeloTextField);
		modeloTextField.setColumns(10);
		
		lblCantidad = new JLabel("Cantidad:");
		lblCantidad.setForeground(Color.DARK_GRAY);
		lblCantidad.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblCantidad.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCantidad.setBounds(10, 101, 63, 14);
		northLeftPanel.add(lblCantidad);
		
		cantidadTextField = new JTextField();
		cantidadTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		cantidadTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		cantidadTextField.setColumns(10);
		cantidadTextField.setBounds(83, 94, 109, 26);
		cantidadTextField.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent ex){
				if(ex.getKeyCode() == KeyEvent.VK_ENTER ){
					addToWhisList();	
				}
			}

			
		});
		northLeftPanel.add(cantidadTextField);
		
		lblPrecioUnitario = new JLabel("Precio U. (MXN): ");
		lblPrecioUnitario.setForeground(Color.DARK_GRAY);
		lblPrecioUnitario.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblPrecioUnitario.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrecioUnitario.setBounds(205, 62, 109, 14);
		northLeftPanel.add(lblPrecioUnitario);
		
		precioTextField = new JTextField();
		precioTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		precioTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		precioTextField.setEditable(false);
		precioTextField.setColumns(10);
		precioTextField.setBounds(531, 55, 89, 26);
		northLeftPanel.add(precioTextField);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(390, 94, 89, 26);
		btnAgregar.addActionListener(e -> {
				addToWhisList();
		});
		northLeftPanel.add(btnAgregar);

        otroBtn= new JButton("No en Inventario");
        otroBtn.setBounds(490, 94, 125, 26);
        otroBtn.addActionListener ( e -> {
            addNotListedPrdouct();
        });
        northLeftPanel.add(otroBtn);
		
		lblExistencias = new JLabel("Existencias: ");
		lblExistencias.setForeground(Color.DARK_GRAY);
		lblExistencias.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblExistencias.setHorizontalAlignment(SwingConstants.RIGHT);
		lblExistencias.setBounds(225, 101, 89, 14);
		northLeftPanel.add(lblExistencias);
		
		existenciasTextField = new JTextField();
		existenciasTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		existenciasTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		existenciasTextField.setEditable(false);
		existenciasTextField.setColumns(10);
		existenciasTextField.setBounds(316, 94, 52, 26);
		northLeftPanel.add(existenciasTextField);
		
		lblPrecioUusd = new JLabel("Precio U. (USD): ");
		lblPrecioUusd.setForeground(Color.DARK_GRAY);
		lblPrecioUusd.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrecioUusd.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblPrecioUusd.setBounds(422, 62, 109, 14);
		northLeftPanel.add(lblPrecioUusd);
		
		precioPesosTextField = new JTextField();
		precioPesosTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		precioPesosTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		precioPesosTextField.setEditable(false);
		precioPesosTextField.setColumns(10);
		precioPesosTextField.setBounds(316, 55, 96, 26);
		northLeftPanel.add(precioPesosTextField);
		
		lblParte_1 = new JLabel("# Parte:");
		lblParte_1.setForeground(Color.DARK_GRAY);
		lblParte_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblParte_1.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblParte_1.setBounds(10, 62, 63, 14);
		northLeftPanel.add(lblParte_1);
		
		codigoTextField = new JTextField();
		codigoTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		codigoTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		codigoTextField.setColumns(10);
		codigoTextField.setBounds(83, 15, 109, 26);
		codigoTextField.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					codigoTextFieldSearch();
				}
			}
		});
		northLeftPanel.add(codigoTextField);
		
		northRightPanel = new JPanel();
		northRightPanel.setBackground(SystemColor.control);
		northRightPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		northRightPanel.setPreferredSize(new Dimension(450, 150));
		northPanel.add(northRightPanel);
		northRightPanel.setLayout(null);
		
		lblFecha = new JLabel("Fecha:");
		lblFecha.setForeground(Color.DARK_GRAY);
		lblFecha.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblFecha.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFecha.setBounds(245, 28, 46, 14);
		northRightPanel.add(lblFecha);
		
		lblVenta = new JLabel("Venta Numero:");
		lblVenta.setForeground(Color.DARK_GRAY);
		lblVenta.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblVenta.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVenta.setBounds(10, 28, 106, 14);
		northRightPanel.add(lblVenta);
		
		lblHora = new JLabel("Descuento:");
		lblHora.setForeground(Color.DARK_GRAY);
		lblHora.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblHora.setHorizontalAlignment(SwingConstants.RIGHT);
		lblHora.setBounds(0, 89, 116, 14);
		northRightPanel.add(lblHora);
		
		fechaTextField = new JTextField();
		fechaTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		fechaTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		fechaTextField.setColumns(10);
		fechaTextField.setBounds(301, 22, 139, 28);
		Date fecha = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd / MM / yyyy");
		fechaTextField.setText(dateFormat.format(fecha));
		northRightPanel.add(fechaTextField);
		
		
		descuentoTextField = new JTextField("0.0");
		descuentoTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		descuentoTextField.setToolTipText("Define un descuento global.");
		descuentoTextField.setColumns(10);
		descuentoTextField.setBounds(126, 83, 68, 28);
		northRightPanel.add(descuentoTextField);
		
		rdbtnTicket = new JRadioButton("Ticket");
		rdbtnTicket.setForeground(Color.DARK_GRAY);
		rdbtnTicket.setFont(new Font("Lao UI", Font.BOLD, 13));
		rdbtnTicket.setBounds(275, 85, 76, 23);
		northRightPanel.add(rdbtnTicket);
		
		rdbtnFactura = new JRadioButton("Factura");
		rdbtnFactura.setForeground(Color.DARK_GRAY);
		rdbtnFactura.setFont(new Font("Lao UI", Font.BOLD, 13));
		rdbtnFactura.setBounds(353, 85, 91, 23);
		northRightPanel.add(rdbtnFactura);
		
		ventaNumTextField = new JTextField();
		ventaNumTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		ventaNumTextField.setHorizontalAlignment(SwingConstants.RIGHT);
		ventaNumTextField.setColumns(10);
		ventaNumTextField.setBounds(126, 22, 109, 28);
		setNumeroVenta();
		northRightPanel.add(ventaNumTextField);
		
		JLabel label = new JLabel("%");
		label.setForeground(Color.DARK_GRAY);
		label.setFont(new Font("Lao UI", Font.BOLD, 16));
		label.setBounds(194, 84, 41, 21);
		northRightPanel.add(label);
		
		midPanel = new JPanel();
		midPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contentPane.add(midPanel, BorderLayout.CENTER);
		midPanel.setLayout(new BoxLayout(midPanel, BoxLayout.Y_AXIS));
		
		
		
		model.addColumn("# Parte");
		model.addColumn("Articulo");
		model.addColumn("Descripcion");
		model.addColumn("Color");
		model.addColumn("Talla");
		model.addColumn("Cantidad");
		model.addColumn("Desc.");
		model.addColumn("Precio U.(MXN)");
		model.addColumn("Importe");
		
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
	    rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
		
		UIDefaults defaults = UIManager.getLookAndFeelDefaults();
		if (defaults.get("Table.alternateRowColor") == null)
		    defaults.put("Table.alternateRowColor", new Color(235, 235, 240));
		
		
		table = new JTable(model);
		table.setShowGrid(false);
	    table.setShowVerticalLines(true);
		table.setPreferredScrollableViewportSize(new Dimension(290, 120));
		table.setFillsViewportHeight(true);
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 13));
		
		table.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(7).setCellRenderer( rightRenderer );
		table.getColumnModel().getColumn(8).setCellRenderer( rightRenderer );
		
		table.getColumnModel().getColumn(2).setPreferredWidth(225);
		table.getColumnModel().getColumn(1).setPreferredWidth(100);
		
//		table.setShowGrid(false);
//	    table.setShowVerticalLines(true);
		
		TableCellRenderer rendererFromHeader = table.getTableHeader().getDefaultRenderer();
	    JLabel headerLabel = (JLabel) rendererFromHeader;
	    headerLabel.setHorizontalAlignment(JLabel.CENTER);
	    
	
		
		scrollPane = new JScrollPane(table);
		midPanel.add(scrollPane);
		
		sumatoriaPanel = new JPanel();
		sumatoriaPanel.setBackground(SystemColor.scrollbar);
		midPanel.add(sumatoriaPanel);
		sumatoriaPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		actionButtonPanel = new JPanel();
		actionButtonPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		actionButtonPanel.setBackground(SystemColor.control);
		actionButtonPanel.setPreferredSize(new Dimension(680, 150));
		sumatoriaPanel.add(actionButtonPanel);
		
		borrarBtn = new JButton("Borrar");
		borrarBtn.setBounds(5, 5, 73, 33);
		borrarBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				repaintTotals();
			}
		});
		actionButtonPanel.setLayout(null);
		actionButtonPanel.add(borrarBtn);
		
		editarBtn = new JButton("Editar");
		editarBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editListItem();
				
			}
		});
		editarBtn.setBounds(85, 5, 68, 33);
		actionButtonPanel.add(editarBtn);
	
		
		labelPanel = new JPanel();
		labelPanel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		labelPanel.setBackground(SystemColor.inactiveCaptionBorder);
		labelPanel.setLayout(null);
		labelPanel.setPreferredSize(new Dimension(400, 150));
		
		sumatoriaPanel.add(labelPanel);
		
		lblSubTotalT = new JLabel("Sub Total:  $");
		lblSubTotalT.setForeground(Color.BLUE);
		lblSubTotalT.setFont(new Font("Lao UI", Font.BOLD, 26));
		lblSubTotalT.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSubTotalT.setBounds(15, 11, 167, 36);
		labelPanel.add(lblSubTotalT);
		
		lblDescuentoT = new JLabel("Descuento:  $ ");
		lblDescuentoT.setForeground(Color.BLUE);
		lblDescuentoT.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDescuentoT.setFont(new Font("Lao UI", Font.BOLD, 26));
		lblDescuentoT.setBounds(0, 40, 188, 36);
		labelPanel.add(lblDescuentoT);
		
//		lblPagadoT = new JLabel("Pagado:   $ ");
//		lblPagadoT.setForeground(Color.DARK_GRAY);
//		lblPagadoT.setHorizontalAlignment(SwingConstants.RIGHT);
//		lblPagadoT.setFont(new Font("Lao UI", Font.BOLD, 22));
//		lblPagadoT.setBounds(31, 70, 167, 36);
//		labelPanel.add(lblPagadoT);
//		
//		lblCambioT = new JLabel("Cambio:   $ ");
//		lblCambioT.setForeground(Color.DARK_GRAY);
//		lblCambioT.setHorizontalAlignment(SwingConstants.RIGHT);
//		lblCambioT.setFont(new Font("Lao UI", Font.BOLD, 22));
//		lblCambioT.setBounds(31, 100, 167, 36);
//		labelPanel.add(lblCambioT);
		
		lblTotalT = new JLabel("Total:   $ ");
		lblTotalT.setForeground(Color.RED);
		lblTotalT.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTotalT.setFont(new Font("Lao UI", Font.BOLD, 40));
		lblTotalT.setBounds(0, 87, 194, 52);
		labelPanel.add(lblTotalT);
		
		subTotalLbl = new JLabel("0.00");
		subTotalLbl.setForeground(Color.BLUE);
		subTotalLbl.setFont(new Font("Lao UI", Font.BOLD, 26));
		subTotalLbl.setHorizontalAlignment(SwingConstants.RIGHT);
		subTotalLbl.setBounds(218, 15, 158, 28);
		labelPanel.add(subTotalLbl);
		
		descuentoLbl = new JLabel("0.00");
		descuentoLbl.setForeground(Color.BLUE);
		descuentoLbl.setHorizontalAlignment(SwingConstants.RIGHT);
		descuentoLbl.setFont(new Font("Lao UI", Font.BOLD, 26));
		descuentoLbl.setBounds(218, 44, 158, 28);
		labelPanel.add(descuentoLbl);
		
		totalLbl = new JLabel("0.00");
		totalLbl.setForeground(Color.RED);
		totalLbl.setHorizontalAlignment(SwingConstants.RIGHT);
		totalLbl.setFont(new Font("Lao UI", Font.BOLD, 38));
		totalLbl.setBounds(186, 89, 204, 50);
		labelPanel.add(totalLbl);
		
		panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBackground(SystemColor.control);

		midPanel.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		
		cobrarBtn = new JButton("Vender");
		cobrarBtn.setFont(new Font("Lao UI", Font.BOLD, 11));
		cobrarBtn.setPreferredSize(new Dimension(115, 65));
		cobrarBtn.setBackground(Color.WHITE);
		cobrarBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		cobrarBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		cobrarBtn.setIcon(new ImageIcon("src/main/resources/Icons/vender.png"));
		cobrarBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				new CobroFrame(self).setVisible(true);
				CobroFrame cf = new CobroFrame(self);
				cf.setVisible(true);
				MainFrame.desktopPane.add(cf,JDesktopPane.POPUP_LAYER);
				try {
					cf.setSelected(true);
				} catch (PropertyVetoException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		cobrarBtn.requestFocus();
		
		btnNuevaVenta = new JButton("Nueva Venta");
		btnNuevaVenta.setFont(new Font("Lao UI", Font.BOLD, 12));
		btnNuevaVenta.setPreferredSize(new Dimension(115, 70));
		btnNuevaVenta.setBackground(Color.WHITE);
		btnNuevaVenta.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnNuevaVenta.setHorizontalTextPosition(SwingConstants.CENTER);
		btnNuevaVenta.setIcon(new ImageIcon("src/main/resources/Icons/nueva_venta.png"));
		btnNuevaVenta.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				nuevaVentaClear();
			}
		});
		btnNuevaVenta.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent ex){
				if(ex.getKeyCode() == KeyEvent.VK_ENTER ){
					nuevaVentaClear();
				}
			}
		});
		
		panel.add(btnNuevaVenta);
		
		cancelarVentaBtn = new JButton("Cancelar Venta");
		cancelarVentaBtn.setFont(new Font("Lao UI", Font.BOLD, 12));
		cancelarVentaBtn.setPreferredSize(new Dimension(115, 70));
		cancelarVentaBtn.setBackground(Color.WHITE);
		cancelarVentaBtn.setVerticalTextPosition(SwingConstants.BOTTOM);
		cancelarVentaBtn.setHorizontalTextPosition(SwingConstants.CENTER);
		cancelarVentaBtn.setIcon(new ImageIcon("src/main/resources/Icons/cancelar_venta.png"));
		cancelarVentaBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		panel.add(cancelarVentaBtn);
		panel.add(cobrarBtn);
		
		
		
//		horizontalBox = Box.createHorizontalBox();
//		contentPane.add(horizontalBox, BorderLayout.SOUTH);
	}
	
	protected void editListItem() {
		EditListaVentaItemDialog editItem = new EditListaVentaItemDialog(this);
		editItem.setVisible(true);
		com.artisub.diveshop.view.MainFrame.desktopPane.add(editItem,JDesktopPane.POPUP_LAYER);
		try {
			editItem.setSelected(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
	}

	private void numParteTextFieldSearch(){
		String numparte = numParteTextField.getText();
		Properties p = new Properties();
		p.put("numparte", numparte);
		
		try{
			producto = productoService.getByQuery("Producto.findNumParte",p);
			modeloTextField.setText(producto.getArticulo().getNombre()+"  "+producto.getDescripcion()+" "+producto.getColor().getNombre()+"  "+producto.getTalla().getNombre());
			cantidadTextField.setText("1");
			cantidadTextField.requestFocus();
			cantidadTextField.selectAll();
			precioTextField.setText(decimalFormat.format(producto.getPrecio()));
			Integer existencias = producto.getExistencias() - cantidadPorVender(producto.getNumparte());
			existenciasTextField.setText(existencias.toString());
			precioPesosTextField.setText(decimalFormat.format(producto.getPrecio()*com.artisub.diveshop.App.TIPO_DE_CAMBIO));
			codigoTextField.setText(producto.getCodigo());
		}catch(NoResultException ex){
			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog(null, "No hay Resultados", "Sin Resultados", JOptionPane.WARNING_MESSAGE);
			modeloTextField.setText("");
			cantidadTextField.setText("");
			precioTextField.setText("");
			existenciasTextField.setText("");
			precioTextField.setText("");
			codigoTextField.setText("");
			numParteTextField.setText("");
			precioPesosTextField.setText("");
		}
	}
	
	private void codigoTextFieldSearch(){
		String codigo = codigoTextField.getText();
		Properties p = new Properties();
		p.put("codigo", codigo);
		
		try{
			producto = productoService.getByQuery("Producto.findCodigo",p);
			modeloTextField.setText(producto.getArticulo().getNombre()+"  "+producto.getDescripcion()+" "+producto.getColor().getNombre()+"  "+producto.getTalla().getNombre());
			cantidadTextField.setText("1");
			cantidadTextField.requestFocus();
			cantidadTextField.selectAll();
			precioTextField.setText(producto.getPrecio().toString());
			/***********/
			Integer existencias = producto.getExistencias() - cantidadPorVender(producto.getCodigo());
			existenciasTextField.setText(existencias.toString());
			precioPesosTextField.setText(decimalFormat.format(producto.getPrecio()*com.artisub.diveshop.App.TIPO_DE_CAMBIO));
			numParteTextField.setText(producto.getNumparte());
		}catch(NoResultException ex){
			Toolkit.getDefaultToolkit().beep();
			JOptionPane.showMessageDialog(null, "No hay Resultados", "Sin Resultados", JOptionPane.WARNING_MESSAGE);
			modeloTextField.setText("");
			cantidadTextField.setText("");
			precioTextField.setText("");
			existenciasTextField.setText("");
			precioTextField.setText("");
			codigoTextField.setText("");
			numParteTextField.setText("");
			precioPesosTextField.setText("");
		}	
	}
	
	public int setNumeroVenta(){
		Properties p = new Properties();
		Integer numventa = null;
		try{
			venta = ventaService.getByQuery("Venta.findLast",p);
			 numventa = venta.getNumventa()+1;
			ventaNumTextField.setText(numventa.toString());
		}catch(NoResultException e){
			ventaNumTextField.setText("1");
		}
		
		return numventa;
	}
	
	protected void nuevaVentaClear(){
		Integer venta = Integer.parseInt(ventaNumTextField.getText());
		venta++;
		descuentoTextField.setText("0.00");
		modeloTextField.setText("");
		cantidadTextField.setText("");
		precioTextField.setText("");
		existenciasTextField.setText("");
		precioTextField.setText("");
		codigoTextField.setText("");
		numParteTextField.setText("");
		precioPesosTextField.setText("");
		ventaNumTextField.setText(venta.toString());
		subTotalLbl.setText("0.00");
		descuentoLbl.setText("0.00");
		totalLbl.setText("0.00");
		subTotalAC=0.00;
		descuento=0.00;
		totalAC=0.00;
		
		while(model.getRowCount() > 0){
			model.removeRow(0);
		}
		
		
	}
	
	protected void openSearchFrame() {
		SearchFrame sf = new ProductoSearchFrame(this, modeloTextField.getText());
		sf.setVisible(true);
		MainFrame.desktopPane.add(sf,JDesktopPane.POPUP_LAYER);
		try {
			sf.setSelected(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
	}

	protected int cantidadPorVender(String numpart){
		int cant = 0;
		for (Venta v : lista_ventas) {
			if (v.getProducto().getCodigo().equals(numpart)) {
				cant += v.getCantidad();
			}
		}
		return cant;
	}
	
	private void cantidadInsuficienteDialog(){
		JOptionPane.showMessageDialog(this, "Existencias Insuficientes");
		codigoTextField.requestFocus();
		codigoTextField.selectAll();
	}

    private void addNotListedPrdouct(){
        ProductoNoListado pnl = new ProductoNoListado();
        pnl.setReference(this);
        pnl.setVisible(true);
        MainFrame.desktopPane.add(pnl,JDesktopPane.POPUP_LAYER);
        try {
            pnl.setSelected(true);
        } catch (PropertyVetoException e1) {
            e1.printStackTrace();
        }
    }
	
	protected void addToWhisList() {
		Double descuentop = Double.parseDouble(descuentoTextField.getText());
		descuentoTextField.setText(descuentop.toString());
		
		existencias = producto.getExistencias() - cantidadPorVender(producto.getNumparte());
		int cantidad = Integer.parseInt(cantidadTextField.getText());
		int current_exist = Integer.parseInt(existenciasTextField.getText());
		
		if((existencias > 0) && (current_exist >= cantidad)){
			venta = new Venta();
			venta.setCantidad(Short.parseShort(cantidadTextField.getText()));
			venta.setComentarios("");
			venta.setDescuento(descuentop);
			venta.setFeha(new Date());
			venta.setNumventa(Integer.parseInt(ventaNumTextField.getText()));
			venta.setProducto(producto);
			venta.setSubtotal((producto.getPrecio()*com.artisub.diveshop.App.TIPO_DE_CAMBIO)*Double.parseDouble(cantidadTextField.getText()));
			venta.setTotal(venta.getSubtotal()-(venta.getSubtotal()/100)*descuentop);
			
			lista_ventas.add(venta);
			
			// INCREMENTA ACUMULADORES
			subTotalAC+= Double.parseDouble((producto.getPrecio().toString()))*Integer.parseInt(cantidadTextField.getText())*com.artisub.diveshop.App.TIPO_DE_CAMBIO;
//			descuento += (descuento>0.0)?(descuento/100)*subTotalAC-10:0;
			descuento += (descuentop>0)?(descuentop/100)*(Double.parseDouble(producto.getPrecio().toString())*com.artisub.diveshop.App.TIPO_DE_CAMBIO*venta.getCantidad()):0;
			totalAC = (subTotalAC) - descuento;
			subTotalLbl.setText(decimalFormat.format(subTotalAC));
			totalLbl.setText(decimalFormat.format(totalAC));
			
			if(descuento != 0.0)
				descuentoLbl.setText(decimalFormat.format(descuento));
			
			model.insertRow(model.getRowCount(), new Object[] { venta.getProducto().getNumparte(), venta.getProducto().getArticulo().getNombre(),
																venta.getProducto().getDescripcion(),venta.getProducto().getColor().getNombre(), venta.getProducto().getTalla().getNombre(),
																venta.getCantidad(), descuentoTextField.getText(),decimalFormat.format(venta.getProducto().getPrecio()*com.artisub.diveshop.App.TIPO_DE_CAMBIO), decimalFormat.format(venta.getTotal())});
			existencias = producto.getExistencias() - cantidadPorVender(producto.getCodigo());
			existenciasTextField.setText(existencias.toString());
			cantidadTextField.setText("1");
			codigoTextField.requestFocus();
			codigoTextField.selectAll();
		}else{
			cantidadInsuficienteDialog();
			cantidadTextField.setText("1");
//			existenciasTextField.setText(existencias.toString());
		}
		
	}
	
	public void repaintTotals(){
		descuento -= (lista_ventas.get(table.getSelectedRow()).getDescuento()>0.0)?((lista_ventas.get(table.getSelectedRow()).getDescuento())/100)*(lista_ventas.get(table.getSelectedRow()).getProducto().getPrecio().doubleValue())*com.artisub.diveshop.App.TIPO_DE_CAMBIO:0;
		subTotalAC -= (lista_ventas.get(table.getSelectedRow()).getProducto().getPrecio().doubleValue())*com.artisub.diveshop.App.TIPO_DE_CAMBIO;
//		totalAC -= (lista_ventas.get(table.getSelectedRow()).getDescuento()>0.0)?
		totalAC = subTotalAC - descuento;
		lista_ventas.remove(lista_ventas.get(table.getSelectedRow()));
		model.removeRow(table.getSelectedRow());
		existencias++;
		existenciasTextField.setText(existencias.toString());
		subTotalLbl.setText(decimalFormat.format(subTotalAC));
		descuentoLbl.setText(decimalFormat.format(descuento));
		totalLbl.setText(decimalFormat.format(totalAC));
	}

	public List<Venta> getLista_ventas() {
		return lista_ventas;
	}

	public void setLista_ventas(List<Venta> lista_ventas) {
		this.lista_ventas = lista_ventas;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}


	private void close(){
		this.dispose();
	}
}
