package com.artisub.diveshop.view.pos;

import java.awt.Container;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.artisub.diveshop.controller.dao.ClienteDao;
import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.ProductoDao;
import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Producto;

public abstract class SearchFrame extends JInternalFrame {

	private static final long serialVersionUID = -4515877651048073657L;
	
	Container container;
	JPanel searchPanel, buttonPanel;
	JTextField searchField;
	JLabel searchLbl;
	JTable table;
	DefaultTableModel modelo;
	JScrollPane scroll;
	String searchText;
	JButton chooseBtn, cancelBtn;
	public Producto producto;
	public PuntoDeVenta pv;
	public PuntoDeVentaPedido pvp;
	
	public IDAO<Producto> productoService = new ProductoDao();
    public List<Producto> lista_productos = productoService.findAll();
    public IDAO<Cliente> clienteService = new ClienteDao();
	public List<Cliente> clientes = clienteService.findAll();
    
    protected static HashMap<Integer, Integer> posindex = new HashMap<Integer, Integer>();
    
    public DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");
	
	public SearchFrame(PuntoDeVenta pv, String searchText){
		this.searchText = searchText;
		this.pv = pv;
		setBounds(280, 100, 750, 380);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		init();
	}
	
	public SearchFrame(PuntoDeVentaPedido pvp, String searchText){
		this.searchText = searchText;
		this.pvp = pvp;
		setBounds(280, 100, 750, 380);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		init();
	}
	
	public abstract void init();
	
	public abstract void setTableData();
	
	public abstract void descripcionTextFieldSearch();
	
}
