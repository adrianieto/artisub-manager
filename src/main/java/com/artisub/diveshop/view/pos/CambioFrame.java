package com.artisub.diveshop.view.pos;

import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class CambioFrame extends JInternalFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JLabel lblCambio,montoLbl;
	
	private DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");
	
	public CambioFrame() {}
	
	public CambioFrame(Double cambio, String msg) {
		super("Cambio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 200, 479, 155);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		lblCambio = new JLabel(msg);
		lblCambio.setFont(new Font("Tahoma", Font.PLAIN, 38));
		lblCambio.setBounds(10, 21, 177, 67);
		contentPane.add(lblCambio);
		montoLbl = new JLabel("0.00");
		montoLbl.setForeground(Color.RED);
		montoLbl.setHorizontalAlignment(SwingConstants.RIGHT);
		montoLbl.setFont(new Font("Tahoma", Font.PLAIN, 48));
		montoLbl.setBounds(197, 21, 239, 64);
		contentPane.add(montoLbl);
		setFocusable(true);
		
		if(cambio!=null){
			montoLbl.setText(decimalFormat.format(cambio));
		}
		
		
	}

	protected void cobrar() {
		// TODO Auto-generated method stub
		
	}
	
	
}
