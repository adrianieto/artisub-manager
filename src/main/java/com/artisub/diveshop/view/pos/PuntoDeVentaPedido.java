package com.artisub.diveshop.view.pos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Venta;
import com.artisub.diveshop.view.MainFrame;


public class PuntoDeVentaPedido extends PuntoDeVenta {
	
	private static final long serialVersionUID = -8836605126099023610L;
	
	private JTabbedPane tabPane;
	private JPanel clientInfo;
	private JLabel nombrelbl, apellidoslbl, telefonolbl, emaillbl;
	public JTextField nombreTextField, apellidosTextField, telefonoTextField, emailTextField;
	private JButton buscarClienteBtn;
	
	protected Cliente cliente;
	
	public int cliente_id;
	
	private PuntoDeVentaPedido self=this;
	
	public PuntoDeVentaPedido() {
		setTitle("Punto de Venta Sobre Pedido");
		
		clientInfo = new JPanel();
		clientInfo.setBackground(SystemColor.control);
		clientInfo.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		clientInfo.setLayout(null);

		nombrelbl = new JLabel("Nombre: ");
		nombrelbl.setForeground(Color.DARK_GRAY);
		nombrelbl.setFont(new Font("Lao UI", Font.BOLD, 13));
		nombrelbl.setHorizontalAlignment(SwingConstants.RIGHT);
		nombrelbl.setBounds(15, 20, 75, 22);
		clientInfo.add(nombrelbl);
		
		apellidoslbl = new JLabel("Apellidos: ");
		apellidoslbl.setForeground(Color.DARK_GRAY);
		apellidoslbl.setFont(new Font("Lao UI", Font.BOLD, 13));
		apellidoslbl.setHorizontalAlignment(SwingConstants.RIGHT);
		apellidoslbl.setBounds(305, 20, 75, 22);
		clientInfo.add(apellidoslbl);
		
		emaillbl = new JLabel("Email: ");
		emaillbl.setForeground(Color.DARK_GRAY);
		emaillbl.setFont(new Font("Lao UI", Font.BOLD, 13));
		emaillbl.setHorizontalAlignment(SwingConstants.RIGHT);
		emaillbl.setBounds(10, 80, 75, 22);
		clientInfo.add(emaillbl);
		
		telefonolbl = new JLabel("Telefono: ");
		telefonolbl.setForeground(Color.DARK_GRAY);
		telefonolbl.setFont(new Font("Lao UI", Font.BOLD, 13));
		telefonolbl.setHorizontalAlignment(SwingConstants.RIGHT);
		telefonolbl.setBounds(300, 80, 75, 22);
		clientInfo.add(telefonolbl);
		
		nombreTextField = new JTextField();
		nombreTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		nombreTextField.setEditable(false);
		nombreTextField.setBounds(90, 20, 200, 28);
		nombreTextField.setColumns(20);
		clientInfo.add(nombreTextField);
		
		apellidosTextField = new JTextField();
		apellidosTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		apellidosTextField.setEditable(false);
		apellidosTextField.setBounds(380, 20, 210, 28);
		apellidosTextField.setColumns(20);
		clientInfo.add(apellidosTextField);
		
		telefonoTextField = new JTextField();
		telefonoTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		telefonoTextField.setEditable(false);
		telefonoTextField.setBounds(380, 75, 110, 28);
		telefonoTextField.setColumns(15);
		clientInfo.add(telefonoTextField);
		
		emailTextField = new JTextField();
		emailTextField.setFont(new Font("Lao UI", Font.BOLD, 12));
		emailTextField.setEditable(false);
		emailTextField.setBounds(90, 75, 200, 28);
		emailTextField.setColumns(45);
		clientInfo.add(emailTextField);
		
		buscarClienteBtn = new JButton("Buscar Cliente");
		buscarClienteBtn.setBounds(500, 75, 120, 28);
		buscarClienteBtn.addActionListener(e->{
			SearchFrame sf = new ClienteSearchFrame(this, modeloTextField.getText());
			sf.setVisible(true);
			MainFrame.desktopPane.add(sf,JDesktopPane.POPUP_LAYER);
			try {
				sf.setSelected(true);
			} catch (PropertyVetoException e1) {
				e1.printStackTrace();
			}
		});
		clientInfo.add(buscarClienteBtn);
		
		northLeftPanel.setPreferredSize(new Dimension(630, 133));
		tabPane = new JTabbedPane();
		tabPane.addTab("Cliente", clientInfo);
		tabPane.addTab("Venta", northLeftPanel);
		northRightPanel.setPreferredSize(new Dimension(450, 163));
		northPanel.add(tabPane);
		northPanel.add(northRightPanel);
		for(ActionListener al : cobrarBtn.getActionListeners()){
			cobrarBtn.removeActionListener(al);
		}
		
		cobrarBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				CobroFramePedido cfp = new CobroFramePedido(self);
				cfp.setVisible(true);
				MainFrame.desktopPane.add(cfp,JDesktopPane.POPUP_LAYER);
				try {
					cfp.setSelected(true);
				} catch (PropertyVetoException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		nombreTextField.requestFocus();
	}
	
	@Override
	protected void addToWhisList() {
		Double descuentop = Double.parseDouble(descuentoTextField.getText());
		descuentoTextField.setText(descuentop.toString());
		
		existencias = producto.getExistencias() - cantidadPorVender(producto.getCodigo());
		int cantidad = Integer.parseInt(cantidadTextField.getText());
		int current_exist = Integer.parseInt(existenciasTextField.getText());
		
//		if((existencias > 0) && (current_exist >= cantidad)){
			venta = new Venta();
			venta.setCantidad(Short.parseShort(cantidadTextField.getText()));
			venta.setComentarios("");
			venta.setDescuento(descuentop);
			venta.setFeha(new Date());
			venta.setNumventa(Integer.parseInt(ventaNumTextField.getText()));
			venta.setProducto(producto);
			venta.setSubtotal((producto.getPrecio()*com.artisub.diveshop.App.TIPO_DE_CAMBIO)*Double.parseDouble(cantidadTextField.getText()));
			venta.setTotal(venta.getSubtotal()-(venta.getSubtotal()/100)*descuentop);
			
			lista_ventas.add(venta);
			
			// INCREMENTA ACUMULADORES
			subTotalAC+= Double.parseDouble((producto.getPrecio().toString()))*Integer.parseInt(cantidadTextField.getText())*com.artisub.diveshop.App.TIPO_DE_CAMBIO;
//			descuento += (descuento>0.0)?(descuento/100)*subTotalAC-10:0;
			descuento += (descuentop>0)?(descuentop/100)*(Double.parseDouble(producto.getPrecio().toString())*com.artisub.diveshop.App.TIPO_DE_CAMBIO*venta.getCantidad()):0;
			totalAC = (subTotalAC*venta.getCantidad()) - descuento;
			subTotalLbl.setText(decimalFormat.format(subTotalAC));
			totalLbl.setText(decimalFormat.format(totalAC));
			
			if(descuento != 0.0)
				descuentoLbl.setText(decimalFormat.format(descuento));
			
			model.insertRow(model.getRowCount(), new Object[] { venta.getProducto().getNumparte(), venta.getProducto().getArticulo().getNombre(),
																venta.getProducto().getDescripcion(),venta.getProducto().getColor().getNombre(), venta.getProducto().getTalla().getNombre(),
																venta.getCantidad(), descuentoTextField.getText(),venta.getSubtotal(), venta.getTotal()});
//			existencias = producto.getExistencias() - cantidadPorVender(producto.getCodigo());
//			existenciasTextField.setText(existencias.toString());
			cantidadTextField.setText("1");
			codigoTextField.requestFocus();
			codigoTextField.selectAll();
//		}else{
//			cantidadTextField.setText("1");
////			existenciasTextField.setText(existencias.toString());
//		}
		
	}
	
	
	@Override
	protected void openSearchFrame() {
		SearchFrame sf = new ProductoSearchFrame(this, modeloTextField.getText());
		sf.setVisible(true);
		MainFrame.desktopPane.add(sf,JDesktopPane.POPUP_LAYER);
		try {
			sf.setSelected(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
	}
	
}
	