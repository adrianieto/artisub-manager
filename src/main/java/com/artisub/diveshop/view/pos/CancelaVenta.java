package com.artisub.diveshop.view.pos;

import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.ProductoDao;
import com.artisub.diveshop.controller.dao.VentaDao;
import com.artisub.diveshop.model.Producto;
import com.artisub.diveshop.model.Venta;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;


public class CancelaVenta extends JInternalFrame {

	private static final long serialVersionUID = -1125036550137628431L;
	
	private Font font = new Font("Lao UI", Font.BOLD, 12);
	
	private Container container;
	private JPanel panel;
	
	private JLabel numventalbl;
	private JTextField numventa;
	
	private JButton cancelarVenta, cancelarOperacion;
	
	private VentaDao ventaService = new VentaDao();
	private IDAO<Producto> productoService = new ProductoDao();
	
	
	
	public CancelaVenta() {
		super("Cancelacion de Venta", false, true, false, false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 210, 320,220);
		init();
	}
	
	private void init(){
		container = getContentPane();
		container.setLayout(new BorderLayout());
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(BorderFactory.createTitledBorder("Cancelar por numero de venta"));

		
		numventalbl = new JLabel("Numero de Venta:");
		numventalbl.setBounds(20, 35, 130, 26);
		numventalbl.setFont(font);
		numventalbl.setForeground(Color.DARK_GRAY);
		
		numventa = new JTextField();
		numventa.setBounds(150, 35, 100, 26);
		numventa.setFont(font);
        numventa.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER){
                    List<Venta> ventasCanceladas;
                    try {
                        ventasCanceladas = ventaService.cancelVenta(Integer.parseInt(numventa.getText()));
                        for (Venta v : ventasCanceladas) {
                            resetInventory(v);
                        }

                    } catch (NumberFormatException ex) {
                        ex.getStackTrace();
                    }
                    closeFrame();

                }
            }
        });
		
		cancelarVenta = new JButton("Cancelar Venta");
		cancelarVenta.setFont(font);
		cancelarVenta.setBounds(20,100,130, 70);
		cancelarVenta.setBackground(Color.WHITE);
		cancelarVenta.setVerticalTextPosition(SwingConstants.BOTTOM);
		cancelarVenta.setHorizontalTextPosition(SwingConstants.CENTER);
		cancelarVenta.setIcon(new ImageIcon("src/main/resources/Icons/cancelar_venta.png"));
        cancelarVenta.addActionListener((e) -> {
                        List<Venta> ventasCanceladas;
                        try {
                            ventasCanceladas = ventaService.cancelVenta(Integer.parseInt(numventa.getText()));
                            for (Venta v : ventasCanceladas) {
                                resetInventory(v);
                            }

                        } catch (NumberFormatException ex) {
                            ex.getStackTrace();
                        }
                        closeFrame();

                });

        cancelarOperacion = new JButton("Salir");
        cancelarOperacion.setFont(font);
        cancelarOperacion.setBounds(160,100,130, 70);
        cancelarOperacion.setBackground(Color.WHITE);
        cancelarOperacion.setVerticalTextPosition(SwingConstants.BOTTOM);
        cancelarOperacion.setHorizontalTextPosition(SwingConstants.CENTER);
        cancelarOperacion.setIcon(new ImageIcon("src/main/resources/Icons/borrar.png"));
        cancelarOperacion.addActionListener((e)->{
            closeFrame();
        });
		
		
		panel.add(numventalbl);
		panel.add(numventa);
		panel.add(cancelarVenta);
        panel.add(cancelarOperacion);
		container.add(panel);
		
	}
	
	@SuppressWarnings("unused")
	private void resetInventory(Venta venta){
		Producto producto = null;
		producto = productoService.findById(venta.getProducto().getId());
		producto.setExistencias(producto.getExistencias() + venta.getCantidad());
		productoService.update(producto);
	}

	private void closeFrame(){
		this.dispose();
	}

}