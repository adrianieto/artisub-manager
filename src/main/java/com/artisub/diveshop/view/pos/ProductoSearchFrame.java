package com.artisub.diveshop.view.pos;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.persistence.NoResultException;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.ProductoDao;
import com.artisub.diveshop.model.Producto;

public class ProductoSearchFrame extends SearchFrame{
	
	private static final long serialVersionUID = 758368571721950688L;
	
	

	public ProductoSearchFrame(PuntoDeVenta pv, String searchText) {
		super(pv, searchText);
	}
	

	@Override
	public void init() {
		container = getContentPane();
		container.setLayout(new BorderLayout());
		
		searchPanel = new JPanel(new FlowLayout());
		
		searchField = new JTextField(25);
		searchLbl = new JLabel("Buscar: ");
		
		modelo = new DefaultTableModel();
		modelo.addColumn("ID");
		modelo.addColumn("Marca");
		modelo.addColumn("Articulo");
		modelo.addColumn("Modelo");
		modelo.addColumn("Color");
		modelo.addColumn("Talla");
		modelo.addColumn("Existencias");
		modelo.addColumn("Precio MXN");
		
		
		table = new JTable(modelo);
		table.setShowGrid(false);
	    table.setShowVerticalLines(true);
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	    centerRenderer.setHorizontalAlignment( JLabel.CENTER );
	    table.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(0).setPreferredWidth(10);
	    table.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(1).setPreferredWidth(20);
	    table.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(2).setPreferredWidth(30);
	    table.getColumnModel().getColumn(3).setPreferredWidth(120);
	    table.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(4).setPreferredWidth(15);
	    table.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(5).setPreferredWidth(15);
	    table.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(6).setPreferredWidth(15);
	    table.getColumnModel().getColumn(7).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(7).setPreferredWidth(15);
	    
	    
		scroll = new JScrollPane(table);
		
		setTableData();
		
		final TableRowSorter<TableModel> sorter;
	    sorter = new TableRowSorter<TableModel>(modelo);
	    table.setRowSorter(sorter);
	    
	    searchField.setText(searchText);
	    String expr = searchText.toUpperCase();
        sorter.setRowFilter(RowFilter.regexFilter(expr));
        sorter.setSortKeys(null);
	        
        
	    int i = 0;
		for(Producto p : lista_productos){
			posindex.put(p.getId(), i);
			i++;
		}
	    
	    searchField.addKeyListener(new KeyAdapter() {
	    	public void keyPressed(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					String expr = searchField.getText().toUpperCase();
			        sorter.setRowFilter(RowFilter.regexFilter(expr));
			        sorter.setSortKeys(null);
				}
	    	}
		});
	    
	    chooseBtn = new JButton("Escoger");
	    chooseBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			    descripcionTextFieldSearch();
				dispose();
			}
		});
	    
	    cancelBtn = new JButton("Cancelar");
	    cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
	    
	    buttonPanel = new JPanel(new FlowLayout());
	    buttonPanel.setBackground(SystemColor.scrollbar);
	    buttonPanel.add(chooseBtn);
	    buttonPanel.add(cancelBtn);
	    
		searchPanel.add(searchLbl);
		searchPanel.setBackground(SystemColor.scrollbar);
		searchPanel.add(searchField);
		container.add(searchPanel, BorderLayout.PAGE_START);
		container.add(scroll, BorderLayout.CENTER);
		container.add(buttonPanel, BorderLayout.PAGE_END);
		
	}

	@Override
	public void setTableData() {
		for(Producto p : lista_productos){
			modelo.insertRow(modelo.getRowCount(), new Object[] {p.getId(),
															     p.getMarca().getNombre(), 
																 p.getArticulo().getNombre(), 
																 p.getDescripcion().toString(),
																 p.getColor().getNombre(), 
																 p.getTalla().getNombre(), 
																 p.getExistencias(), 
																 decimalFormat.format(p.getPrecio()*com.artisub.diveshop.App.TIPO_DE_CAMBIO)});
		}
		
	}
	
	@Override
	public void descripcionTextFieldSearch() {
		try{
			producto = productoService.findById((int) (table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0)));
			pv.producto = producto;
			pv.modeloTextField.setText(producto.getArticulo().getNombre()+"  "+producto.getDescripcion()+" "+producto.getColor().getNombre()+"  "+producto.getTalla().getNombre());
			pv.cantidadTextField.setText("1");
			pv.cantidadTextField.requestFocus();
			pv.cantidadTextField.selectAll();
			pv.precioTextField.setText(decimalFormat.format(producto.getPrecio()));
			Integer existencias = producto.getExistencias() - pv.cantidadPorVender(producto.getNumparte());
			pv.existenciasTextField.setText(existencias.toString());
			pv.codigoTextField.setText(producto.getCodigo());
			pv.numParteTextField.setText(producto.getNumparte());
			pv.precioPesosTextField.setText(decimalFormat.format(producto.getPrecio()* com.artisub.diveshop.App.TIPO_DE_CAMBIO));
			
		}catch(NoResultException ex){
			JOptionPane.showMessageDialog(null, "No hay Resultados", "Sin Resultados", JOptionPane.WARNING_MESSAGE);
			pv.modeloTextField.setText("");
			pv.cantidadTextField.setText("");
			pv.precioTextField.setText("");
			pv.existenciasTextField.setText("");
			pv.precioTextField.setText("");
			pv.codigoTextField.setText("");
			pv.numParteTextField.setText("");
			pv.precioPesosTextField.setText("");
		}
		
	}
}
