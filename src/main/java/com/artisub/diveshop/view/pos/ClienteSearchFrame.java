package com.artisub.diveshop.view.pos;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.artisub.diveshop.controller.dao.ClienteDao;
import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Producto;

public class ClienteSearchFrame extends SearchFrame{
	
	private static final long serialVersionUID = -3864481586325886424L;
	
	

	public ClienteSearchFrame(PuntoDeVentaPedido pvp, String searchText) {
		super(pvp, searchText);
	}

	@Override
	public void init() {
		container = getContentPane();
		container.setLayout(new BorderLayout());
		
		searchPanel = new JPanel(new FlowLayout());
		
		searchField = new JTextField(25);
		searchLbl = new JLabel("Buscar: ");
		
		modelo = new DefaultTableModel();
		modelo.addColumn("ID");
		modelo.addColumn("Nombre");
		modelo.addColumn("Apellido");
		modelo.addColumn("Telefono");
		modelo.addColumn("Email");
		
		
		table = new JTable(modelo);
		table.setShowGrid(false);
	    table.setShowVerticalLines(true);
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	    centerRenderer.setHorizontalAlignment( JLabel.CENTER );
	    
	    table.getColumnModel().getColumn(0).setPreferredWidth(10);
	    table.getColumnModel().getColumn(1).setPreferredWidth(30);
	    table.getColumnModel().getColumn(2).setPreferredWidth(30);
	    table.getColumnModel().getColumn(3).setPreferredWidth(15);
	    table.getColumnModel().getColumn(4).setPreferredWidth(15);
	    
	    
		scroll = new JScrollPane(table);
		
		setTableData();
		
		final TableRowSorter<TableModel> sorter;
	    sorter = new TableRowSorter<TableModel>(modelo);
	    table.setRowSorter(sorter);
	    
	    searchField.setText(searchText);
	    String expr = searchText.toUpperCase();
        sorter.setRowFilter(RowFilter.regexFilter(expr));
        sorter.setSortKeys(null);
	        
        
//	    int i = 0;
//		for(Producto p : lista_productos){
//			posindex.put(p.getId(), i);
//			i++;
//		}
	    
	    searchField.addKeyListener(new KeyAdapter() {
	    	public void keyPressed(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					String expr = searchField.getText().toUpperCase();
			        sorter.setRowFilter(RowFilter.regexFilter(expr));
			        sorter.setSortKeys(null);
				}
	    	}
		});
	    
	    chooseBtn = new JButton("Escoger");
	    chooseBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			    descripcionTextFieldSearch();
				dispose();
			}
		});
	    
	    cancelBtn = new JButton("Cancelar");
	    cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
	    
	    buttonPanel = new JPanel(new FlowLayout());
	    buttonPanel.setBackground(SystemColor.scrollbar);
	    buttonPanel.add(chooseBtn);
	    buttonPanel.add(cancelBtn);
	    
		searchPanel.add(searchLbl);
		searchPanel.setBackground(SystemColor.scrollbar);
		searchPanel.add(searchField);
		container.add(searchPanel, BorderLayout.PAGE_START);
		container.add(scroll, BorderLayout.CENTER);
		container.add(buttonPanel, BorderLayout.PAGE_END);
		

		
	}

	@Override
	public void setTableData() {
		for(Cliente c : clientes){
			modelo.insertRow(modelo.getRowCount(), new Object[] {c.getId(),
															     c.getNombre(), 
															     c.getApellidos(),
															     c.getTelefono(),
																 c.getEmail()
																 });
		}
		
	}

	@Override
	public void descripcionTextFieldSearch() {
		Cliente cliente = clienteService.findById((int)(table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0)));
		pvp.cliente=cliente;
		pvp.nombreTextField.setText(cliente.getNombre());
		pvp.apellidosTextField.setText(cliente.getApellidos());
		pvp.telefonoTextField.setText(cliente.getTelefono());
		pvp.emailTextField.setText(cliente.getEmail());
	}

	
	
	
}
