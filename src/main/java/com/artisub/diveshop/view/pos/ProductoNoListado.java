package com.artisub.diveshop.view.pos;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.SystemColor;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import com.artisub.diveshop.Referenciable;
import com.artisub.diveshop.controller.dao.ArticuloDao;
import com.artisub.diveshop.controller.dao.ColorsDao;
import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.MarcaDao;
import com.artisub.diveshop.controller.dao.TallaDao;
import com.artisub.diveshop.model.Articulo;
import com.artisub.diveshop.model.Colors;
import com.artisub.diveshop.model.Marca;
import com.artisub.diveshop.model.Producto;
import com.artisub.diveshop.model.Talla;

/**
 * Created by adrian on 20/11/2015.
 */
public class ProductoNoListado extends JInternalFrame implements Referenciable<PuntoDeVenta> {

	private static final long serialVersionUID = -1541561227975432661L;
	private final Font font = new Font("Lao UI", Font.BOLD, 13);
    private PuntoDeVenta puntoDeVenta;
    
    private final IDAO<Articulo> articuloService = new ArticuloDao();
    private final IDAO<Marca> marcaService = new MarcaDao();
    private final IDAO<Colors> colorService = new ColorsDao();
    private final IDAO<Talla> tallaService = new TallaDao();

    private Container container;
    private JPanel panel;
    private JLabel descripcionLbl;
    private JLabel precioLbl;
    private JTextField descripcionTextField;
    private JTextField precioTextField;
    private JButton agregarBtn;
    private JButton cancelatBtn;

    private DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");

    ProductoNoListado(){
        super("Agregar producto no listado", false,true,false,false);
        setBounds(340, 90, 453, 250);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        init();
    }

    private void init(){
        container = getContentPane();
        container.setLayout(null);
        container.setBackground(SystemColor.scrollbar);
        panel = new JPanel();
        panel.setBounds(5,5,433,150);
        panel.setLayout(null);
        panel.setBorder(new EtchedBorder());

        precioLbl = new JLabel("Precio (MXN): ");
        precioLbl.setFont(font);
        precioLbl.setBounds(15,30,100,26);
        precioLbl.setHorizontalAlignment(SwingConstants.RIGHT);
        precioLbl.setForeground(Color.DARK_GRAY);

        precioTextField = new JTextField();
        precioTextField.setFont(font);
        precioTextField.setBounds(120,30,100,26);

        descripcionLbl = new JLabel("Descripcion: ");
        descripcionLbl.setFont(font);
        descripcionLbl.setBounds(15,85,100,26);
        descripcionLbl.setHorizontalAlignment(SwingConstants.RIGHT);
        descripcionLbl.setForeground(Color.DARK_GRAY);

        descripcionTextField = new JTextField();
        descripcionTextField.setFont(font);
        descripcionTextField.setBounds(120,85,300,26);

        agregarBtn = new JButton("Agregar");
        agregarBtn.setBounds(310,175,100,26);
        agregarBtn.addActionListener(e->{
        	agregarALista();
        });

        cancelatBtn = new JButton("Cancelar");
        cancelatBtn.setBounds(30,175,100,26);
        cancelatBtn.addActionListener(e->{
        	closeFrame();
        });

        panel.add(precioLbl);
        panel.add(precioTextField);
        panel.add(descripcionLbl);
        panel.add(descripcionTextField);

        container.add(panel);
        container.add(agregarBtn);
        container.add(cancelatBtn);

    }

    private void agregarALista(){
    	Double precio = Double.parseDouble(precioTextField.getText());
    	
        Producto producto = new Producto();
//        producto.setId(99999);
        producto.setNumparte("ARTNL");
        producto.setExistencias(99);
        producto.setCodigo("0000000001");
        producto.setArticulo(articuloService.findById(1));
        producto.setMarca(marcaService.findById(2));
        producto.setColor(colorService.findById(6));
        producto.setTalla(tallaService.findById(1));
        producto.setDescripcion(descripcionTextField.getText().toUpperCase());
        producto.setPrecio(precio/com.artisub.diveshop.App.TIPO_DE_CAMBIO);
        producto.setProd_no_listado(true);
        
        puntoDeVenta.producto = producto;	
        puntoDeVenta.codigoTextField.setText(producto.getCodigo());
        puntoDeVenta.numParteTextField.setText(producto.getNumparte());
        puntoDeVenta.existenciasTextField.setText(producto.getExistencias().toString());
        puntoDeVenta.modeloTextField.setText(producto.getDescripcion());
        puntoDeVenta.cantidadTextField.setText("1");
        puntoDeVenta.cantidadTextField.requestFocus();
        puntoDeVenta.cantidadTextField.selectAll();
        puntoDeVenta.precioPesosTextField.setText(decimalFormat.format(precio));
        precio = precio/com.artisub.diveshop.App.TIPO_DE_CAMBIO;
        puntoDeVenta.precioTextField.setText(decimalFormat.format(precio));

        closeFrame();    
    }

    private void closeFrame(){
        this.dispose();
    }

    @Override
    public void setReference(PuntoDeVenta puntoDeVenta) {
        this.puntoDeVenta=puntoDeVenta;
    }
}
