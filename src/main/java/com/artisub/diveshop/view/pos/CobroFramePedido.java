package com.artisub.diveshop.view.pos;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.util.Date;

import javax.swing.JDesktopPane;
import javax.swing.SwingConstants;

import com.artisub.diveshop.Referenciable;
import com.artisub.diveshop.controller.dao.ClienteDao;
import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.PedidoDao;
import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Formapago;
import com.artisub.diveshop.model.Pedido;
import com.artisub.diveshop.model.Venta;

public class CobroFramePedido extends CobroFrame{

	private static final long serialVersionUID = 48346114265844511L;
	
	IDAO<Cliente> clienteService = new ClienteDao();
	IDAO<Pedido> pedidoService = new PedidoDao();
	
	private PuntoDeVentaPedido ventaPedido;
	
	public CobroFramePedido(PuntoDeVentaPedido ventaPedido) {
		super(ventaPedido);
		setTitle("Pedido");
		this.ventaPedido=ventaPedido;
		
		lblMontoRecibido.setText("Anticipo: ");
		lblMontoRecibido.setForeground(Color.DARK_GRAY);
		lblMontoRecibido.setFont(new Font("Lao UI", Font.BOLD, 13));
		lblMontoRecibido.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMontoRecibido.setBounds(10, 50, 113, 14);
		panel.add(lblMontoRecibido);
	}
	
	@Override
	protected void persistirVenta(){
		Formapago fp = null;
		for(Formapago f : lista_formapago){
			if(f.getFormapago().compareTo(formPagocomboBox.getSelectedItem().toString()) == 0){
				fp =f;
			}
		}
		
//		List<Cliente> clientes = new ArrayList<Cliente>();
//		
//		Cliente c = clienteService.findById(ventaPedido.cliente_id);
//		c.setNombre(ventaPedido.nombreTextField.getText().toUpperCase());
//		c.setApellidos(ventaPedido.apellidosTextField.getText().toUpperCase());
//		c.setEmail(ventaPedido.emailTextField.getText().toLowerCase());
//		c.setTelefono(ventaPedido.telefonoTextField.getText());
//		clientes.add(c);
//		clienteService.create(c);

		
		//Recorre lista de ventas y guarda una a una cada venta dentro de la lista
		for (Venta v : ventaPedido.lista_ventas) {
			v.setFormapago(fp);
			v.setPedido(true);
			ventaService.create(v);
		}
		
		Pedido pedido = new Pedido();
		pedido.setFormapago(fp);
		pedido.setFecha(new Date());
		pedido.setPedidoEntregado(false);
		pedido.setNumVenta(ventaPedido.lista_ventas.get(0).getNumventa());
		pedido.setAnticipo(Double.parseDouble(pagoTextField.getText()));
		pedido.setCliente(ventaPedido.cliente);
		
		pedidoService.create(pedido);

	}
	
	@Override
	protected void cobrar(){
		Double pago = Double.parseDouble(pagoTextField.getText());
		Double cambio =  ventaPedido.totalAC - pago;

		//Persiste la lista de ventas
		persistirVenta();

		ventaPedido.lista_ventas.clear();
		ventaPedido.btnNuevaVenta.requestFocus();
		
		CambioFrame cf = new CambioFrame(cambio, "Restan: $ ");
		cf.setVisible(true);
		com.artisub.diveshop.view.MainFrame.desktopPane.add(cf,JDesktopPane.POPUP_LAYER);
		try {
			cf.setSelected(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
		cf.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e){
				cf.dispose();
				try {
					ventaPedido.setSelected(true);
					ventaPedido.codigoTextField.requestFocus();
					ventaPedido.nuevaVentaClear();
				} catch (PropertyVetoException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		close();
	}
	
	
}
