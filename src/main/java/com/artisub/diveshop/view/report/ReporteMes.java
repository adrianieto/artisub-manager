package com.artisub.diveshop.view.report;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

import com.artisub.diveshop.controller.dao.PedidoDao;
import com.artisub.diveshop.controller.dao.VentaDao;
import com.artisub.diveshop.model.Pedido;
import com.artisub.diveshop.model.Venta;

public class ReporteMes extends JInternalFrame{
	
	private static final long serialVersionUID = 6784878521170865709L;
	
	private double sumsem1=0.0, sumsem2=0.0, sumsem3=0.0, sumsem4=0.0,sumsem5=0.0;	
	private Container container;
	private DefaultCategoryDataset dataset;
	
	private JLabel meslbl, aniolbl, imglbl;
	private JComboBox<String> mesComboBox, anioComboBox;
	
	private VentaDao ventaService = new VentaDao();
	private List<Venta> ventasMes = new ArrayList<>();
	private PedidoDao pedidoService = new PedidoDao();
	private List<Pedido> pedidos = pedidoService.findPedidosByDate(new Date());
	
	private Date date1 = new Date();
	private Date date2 = new Date();
	
	private List<Venta> sem1 = new ArrayList<>();
	private List<Venta> sem2 = new ArrayList<>();
	private List<Venta> sem3 = new ArrayList<>();
	private List<Venta> sem4 = new ArrayList<>();
	private List<Venta> sem5 = new ArrayList<>();
	
	private JLabel efectivolbl, pagarelbl, chequelbl, depositolbl, grantotallbl;
	private JLabel efectivototal, pagaretotal, chequetotal, depositototal, grantotal;
	
	private Double efectivosum=0.0;
	private Double tarjetasum=0.0;
	private Double chequesum=0.0;
	private Double depositosum=0.0;
	private Double grantotalsum=0.0;
	
	private DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");
	
	private ChartPanel cp;
	
	private String[] meses = {"ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
	private String[] anios = {"2015","2016","2017","2018","2019","2020"};
	
	public ReporteMes(){
		super("Reporte de Mes", true, true, false, true);
		setBounds(190,10,640, 650);
		setLayout(null);
		init(date1, date2);
	}
		
	private void init(Date date1, Date date2){
		container = getContentPane();
		
		meslbl = new JLabel("Mes: ");
		meslbl.setBounds(15, 5, 30, 25);
		aniolbl = new JLabel("A�o: ");
		aniolbl.setBounds(180, 5, 30, 25);
		anioComboBox = new JComboBox<>();
		anioComboBox.setBounds(210, 5, 80, 25);
		mesComboBox = new JComboBox<>();
		mesComboBox.setBounds(45, 5, 120, 25);
		fillComboBox();
		mesComboBox.setSelectedIndex(new Date().getMonth());
		mesComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				date1.setMonth(mesComboBox.getSelectedIndex());
				date2.setMonth(mesComboBox.getSelectedIndex());
				cp.setChart(constructChart(date1, date2));
				cp.repaint();
			}
		});
		
		efectivolbl = new JLabel("Total Efectivo:");
		efectivolbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		efectivolbl.setHorizontalAlignment(SwingConstants.RIGHT);
		efectivolbl.setBounds(50, 470, 105, 20);
		
		pagarelbl = new JLabel("Total Tarjeta:");
		pagarelbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		pagarelbl.setHorizontalAlignment(SwingConstants.RIGHT);
		pagarelbl.setBounds(50, 500, 105, 20);
		
		chequelbl = new JLabel("Total Cheque:");
		chequelbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		chequelbl.setHorizontalAlignment(SwingConstants.RIGHT);
		chequelbl.setBounds(330, 470, 105, 20);
		
		depositolbl = new JLabel("Total Deposito:");
		depositolbl.setFont(new Font("Tahoma", Font.BOLD, 14));
		depositolbl.setHorizontalAlignment(SwingConstants.RIGHT);
		depositolbl.setBounds(330, 500, 105, 20);
		
		efectivototal = new JLabel("$  0.00");
		efectivototal.setFont(new Font("Tahoma", Font.BOLD, 14));
		efectivototal.setForeground(Color.RED);
		efectivototal.setHorizontalAlignment(SwingConstants.RIGHT);
		efectivototal.setBounds(175, 470, 100, 20);

		pagaretotal = new JLabel("$  0.00");
		pagaretotal.setFont(new Font("Tahoma", Font.BOLD, 14));
		pagaretotal.setForeground(Color.RED);
		pagaretotal.setHorizontalAlignment(SwingConstants.RIGHT);
		pagaretotal.setBounds(175, 500, 100, 20);
		
		chequetotal = new JLabel("$  0.00");
		chequetotal.setFont(new Font("Tahoma", Font.BOLD, 14));
		chequetotal.setForeground(Color.RED);
		chequetotal.setHorizontalAlignment(SwingConstants.RIGHT);
		chequetotal.setBounds(330+125, 470, 100, 20);
		
		depositototal = new JLabel("$  0.00");
		depositototal.setFont(new Font("Tahoma", Font.BOLD, 14));
		depositototal.setForeground(Color.RED);
		depositototal.setHorizontalAlignment(SwingConstants.RIGHT);
		depositototal.setBounds(330+125, 500, 100, 20);
		
		
		grantotallbl = new JLabel("Gran Total:");
		grantotallbl.setFont(new Font("Tahoma", Font.BOLD, 32));
		grantotallbl.setHorizontalAlignment(SwingConstants.RIGHT);
		grantotallbl.setBounds(30, 550, 250, 50);
		
		grantotal = new JLabel("$  0.00");
		grantotal.setFont(new Font("Tahoma", Font.BOLD, 32));
		grantotal.setForeground(Color.BLUE);
		grantotal.setHorizontalAlignment(SwingConstants.RIGHT);
		grantotal.setBounds(290, 550, 250, 50);
		
		imglbl = new JLabel(new ImageIcon("src/main/resources/Icons/Billing.png"));
		imglbl.setBounds(550, 10, 48, 48);
		
		cp = new ChartPanel(constructChart(date1, date2));
		cp.setBounds(3, 40, 600, 390);
		container.add(meslbl);
		container.add(mesComboBox);
		container.add(anioComboBox);
		container.add(aniolbl);
		container.add(efectivolbl);
		container.add(pagarelbl);
		container.add(chequelbl);
		container.add(depositolbl);
		container.add(efectivototal);
		container.add(pagaretotal);
		container.add(chequetotal);
		container.add(depositototal);
		container.add(grantotallbl);
		container.add(grantotal);
		container.add(imglbl);
		container.add(cp);
	}
	
	private JFreeChart constructChart(Date date1, Date date2){
		sem1.clear();
		sem2.clear();
		sem3.clear();
		sem4.clear();
		sem5.clear();
		this.sumsem1=0.0;
		this.sumsem2=0.0; 
		this.sumsem3=0.0;
		this.sumsem4=0.0;
		this.sumsem5=0.0;
		
		this.efectivosum=0.0;
		this.tarjetasum=0.0;
		this.chequesum=0.0;
		this.depositosum=0.0;
		this.grantotalsum=0.0;
		date1.setDate(1);
		date1.setHours(0);
		date1.setMinutes(0);
		date1.setSeconds(0);
		date2.setDate(31);
		date2.setHours(23);
		date2.setMinutes(59);
		date2.setSeconds(59);
		
		ventasMes = ventaService.findSales(date1, date2);
		
		for(Venta v : ventasMes){
			if(v.getFormapago().getFormapago().equals("EFECTIVO")){
				efectivosum = efectivosum + v.getTotal();
			}
			if(v.getFormapago().getFormapago().equals("TARJETA")){
				tarjetasum = tarjetasum + v.getTotal();
			}
			if(v.getFormapago().getFormapago().equals("CHEQUE")){
				chequesum = chequesum + v.getTotal();
			}
			if(v.getFormapago().getFormapago().equals("DEPOSITO")){
				depositosum = depositosum + v.getTotal();
			}
			
			if(v.getFeha().getDate()>=1 && v.getFeha().getDate() <=7){
				sem1.add(v);
			}else if(v.getFeha().getDate()>=8 && v.getFeha().getDate() <=13){
				sem2.add(v);
			}else if(v.getFeha().getDate()>=14 && v.getFeha().getDate() <=19){
				sem3.add(v);
			}else if(v.getFeha().getDate()>=20 && v.getFeha().getDate() <=27){
				sem4.add(v);
			}else if(v.getFeha().getDate()>=28 && v.getFeha().getDate() <=31){
				sem5.add(v);
			}
		}
		
		for(Venta v : sem1){
			sumsem1 = sumsem1 + v.getTotal();;
		}
		for(Venta v : sem2){
			sumsem2 = sumsem2 + v.getTotal();
		}
		for(Venta v : sem3){
			sumsem3 = sumsem3 + v.getTotal();
		}
		for(Venta v : sem4){
			sumsem4 = sumsem4 + v.getTotal();
		}
		for(Venta v : sem5){
			sumsem5 = sumsem5 + v.getTotal();
		}
		
		
		
		dataset = new DefaultCategoryDataset();
		dataset.setValue(sumsem1, "Ventas", "Semana 1");
		dataset.setValue(sumsem2, "Ventas", "Semana 2");
		dataset.setValue(sumsem3, "Ventas", "Semana 3");
		dataset.setValue(sumsem4, "Ventas", "Semana 4");
		dataset.setValue(sumsem5, "Ventas", "Semana 5");
		
		grantotalsum = efectivosum+tarjetasum+chequesum+depositosum;
		
		efectivototal.setText("$ "+decimalFormat.format(efectivosum));
		pagaretotal.setText("$ "+decimalFormat.format(tarjetasum));
		chequetotal.setText("$ "+decimalFormat.format(chequesum));
		depositototal.setText("$ "+decimalFormat.format(depositosum));
		grantotal.setText("$ "+decimalFormat.format(grantotalsum));
		
		JFreeChart chart = ChartFactory.createBarChart3D( "VENTAS DE " + meses[date1.getMonth()],
		"Semana", "Ventas ($)", dataset, PlotOrientation.VERTICAL, true, true, false );
		
		CategoryPlot plot = chart.getCategoryPlot();
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		Color color = new Color(79, 129, 189);
		renderer.setSeriesPaint(0, color);
		
		return chart;
	}
	
	private void sumaPedidosAnticipo(){
//		if(){
//			
//		}
//		
//		if((v.getFeha().getDate()>=1 && v.getFeha().getDate() <=7)&&v.isPedido()){
//			for(Pedido p : pedido){
//				sumsem1 = sumsem1 + p.getAnticipo();
//			}
//		}else if(v.getFeha().getDate()>=8 && v.getFeha().getDate() <=13){
//			for(Pedido p : pedido){
//				sumsem2 = sumsem2 + p.getAnticipo();
//			}
//		}else if(v.getFeha().getDate()>=14 && v.getFeha().getDate() <=19){
//			for(Pedido p : pedido){
//				sumsem3 = sumsem3 + p.getAnticipo();
//			}
//		}else if(v.getFeha().getDate()>=20 && v.getFeha().getDate() <=27){
//			for(Pedido p : pedido){
//				sumsem4 = sumsem4 + p.getAnticipo();
//			}
//		}else if(v.getFeha().getDate()>=28 && v.getFeha().getDate() <=31){
//			for(Pedido p : pedido){
//				sumsem5 = sumsem5 + p.getAnticipo();
//			}
//		}
//			
	}
	
	private void fillComboBox(){
		for(String mes : meses){
			mesComboBox.addItem(mes);
		}
		for(String anio : anios){
			anioComboBox.addItem(anio);
		}
	}

}
