package com.artisub.diveshop.view.report;

import java.awt.Container;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JInternalFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.MarcaDao;
import com.artisub.diveshop.controller.dao.VentaDao;
import com.artisub.diveshop.model.Marca;
import com.artisub.diveshop.model.Venta;

public class MarketshareFrame extends JInternalFrame {

	private static final long serialVersionUID = 3005060510684163338L;
	
	private VentaDao ventaService = new VentaDao();
	private IDAO<Marca> marcaService = new MarcaDao();
	
	private Container container;
	private ChartPanel cp;
	
	public MarketshareFrame() {
		super("Market Share", true, true, true, true);
		setBounds(100,10,990,620);
		setLayout(null);
		init();
	}
	
	private void init(){
		container = getContentPane();
		
		cp = new ChartPanel(createChart());
		cp.setBounds(3, 10, 980, 570);
		
		container.add(cp);
	}
	
	
	private JFreeChart createChart(){
		DefaultPieDataset dataset = new DefaultPieDataset();
		List<Marca> marcas = marcaService.findAll();
		List<List<Venta>> ventas = new ArrayList<List<Venta>>();
		Map<String, Double> marcas_map = new HashMap<String, Double>();
		Double sum = 0.0;
		
		for(Marca marca : marcas){
			ventas.add(ventaService.findByMarca(marca));
			marcas_map.put(marca.getNombre(), 0.0);
		}
		
		for(List<Venta> venta : ventas){
			for(Venta v : venta){
				sum = marcas_map.get(v.getProducto().getMarca().getNombre());
				sum =  sum + v.getTotal();
				marcas_map.replace(v.getProducto().getMarca().getNombre(), sum);
			}
		}
		
		for(Map.Entry<String, Double> map : marcas_map.entrySet()){
			dataset.setValue(map.getKey(), map.getValue());
		}
		
		JFreeChart chart = ChartFactory.createPieChart(
	            "Marketshare",  // chart title
	            dataset,             // data
	            false,               // include legend
	            true,
	            false
	        );
		
		PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelFont(new Font("SansSerif", Font.PLAIN, 11));
        plot.setNoDataMessage("No data available");
        plot.setCircular(false);
        plot.setLabelGap(0.08);
        
        return chart;
		
	}

}
