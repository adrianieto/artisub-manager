package com.artisub.diveshop.view.report;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.artisub.diveshop.controller.dao.PedidoDao;
import com.artisub.diveshop.controller.dao.VentaDao;
import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Pedido;
import com.artisub.diveshop.model.Venta;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;


public class CorteDeCajaFrame  extends JInternalFrame {
	
	private static final long serialVersionUID = 8252762678500057213L;
	
	private Double efectivoTotal=0.0, pagareTotal=0.0, chequeTotal=0.0, despositoTotal=0.0, granTotal=0.0;
	
	private JTable table;
	private JPanel panel;
	private JScrollPane scroll;
	
	private JLabel lblEfectivo, lblPagare, lblCheque, lblDolares, lblGranTotal, lblFecha,
				efectivoLbl, pagareLbl, chequeLbl, depositoLbl, granTotalLbl;
	
	private JButton btnAceptar;
	
	private DefaultTableModel model = new DefaultTableModel();
	
	private VentaDao ventaService = new VentaDao();
	private PedidoDao pedidoService = new PedidoDao();
	
	private Date currentDate1 = new Date();
	private Date currentDate2 = new Date();

	private List<Venta> ventas;
	private List<Venta> pedidos;
	protected List<Pedido> clientes_pedido;
	
	private DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");
	
	UtilDateModel dateModel;
	JDatePanelImpl datePanel;
	JDatePickerImpl datePicker;
	
	Date date;
	/**
	 * Contructor
	 */
	public CorteDeCajaFrame() {
	super("Resumen de Ventas",false,true,false,false);
	setBounds(345, 40, 634+40, 620);
	getContentPane().setLayout(null);
	currentDate1.setSeconds(0);
	currentDate1.setMinutes(0);
	currentDate1.setHours(0);
	currentDate2.setSeconds(59);
	currentDate2.setMinutes(59);
	currentDate2.setHours(23);
	ventas  = ventaService.findSales(currentDate1, currentDate2);
	pedidos = ventaService.findPedidosTotales(currentDate1, currentDate2);
	
	dateModel = new UtilDateModel();
	dateModel.setValue(new Date());
	
	datePanel = new JDatePanelImpl(dateModel);
	datePicker = new JDatePickerImpl(datePanel);
	datePicker.setBounds(450, 10, 200, 30);
	datePicker.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			Date selectedDate1 = (Date) datePicker.getModel().getValue();
			selectedDate1.setSeconds(0);
			selectedDate1.setMinutes(0);
			selectedDate1.setHours(0);
			Date selectedDate2 = (Date) datePicker.getModel().getValue();
			selectedDate2.setSeconds(59);
			selectedDate2.setMinutes(59);
			selectedDate2.setHours(23);
			ventas  = ventaService.findSales(selectedDate1, selectedDate2);
			pedidos = ventaService.findPedidosTotales(selectedDate1, selectedDate2);
			model.getDataVector().removeAllElements();
			model.fireTableDataChanged();
			setTableData();
			table.repaint();
			calculateTotals();
		}
	});
	
	lblFecha = new JLabel("Fecha:");
	lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 15));
	lblFecha.setBounds(400,10, 100,30);
	
	panel = new JPanel();
	panel.setBorder(new TitledBorder(null, "Ventas", TitledBorder.CENTER, TitledBorder.TOP, null, null));
	panel.setBounds(10, 50, 595+44, 471+25);
	getContentPane().add(datePicker);
	getContentPane().add(panel);
	getContentPane().add(lblFecha);
	
	panel.setLayout(null);
	
	model.addColumn("Venta #");
	model.addColumn("Cant");
	model.addColumn("Descripcion");
	model.addColumn("Desc %");
	model.addColumn("Sub Total");
	model.addColumn("Total");
	model.addColumn("Forma Pago");
	
	UIDefaults defaults = UIManager.getLookAndFeelDefaults();
	if (defaults.get("Table.alternateRowColor") == null)
	    defaults.put("Table.alternateRowColor", new Color(220, 220, 230));
	
	DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    centerRenderer.setHorizontalAlignment( JLabel.CENTER );
    DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
    rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
	table = new JTable(model);
	table.setShowGrid(false);
    table.setShowVerticalLines(true);
	table.setBounds(10, 27, 575+45, 217);
	
	table.getColumnModel().getColumn(0).setPreferredWidth(20);
	table.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
	table.getColumnModel().getColumn(1).setPreferredWidth(8);
	table.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
	table.getColumnModel().getColumn(2).setPreferredWidth(240);
	table.getColumnModel().getColumn(3).setPreferredWidth(30);
	table.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
	table.getColumnModel().getColumn(4).setPreferredWidth(40);
	table.getColumnModel().getColumn(4).setCellRenderer( rightRenderer );
	table.getColumnModel().getColumn(5).setPreferredWidth(40);
	table.getColumnModel().getColumn(5).setCellRenderer( rightRenderer );
	table.getColumnModel().getColumn(6).setPreferredWidth(50);
	table.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
	
	setTableData();
	scroll = new JScrollPane(table);
	scroll.setBounds(10, 27, 575+45, 217);
	panel.setBackground(Color.WHITE);
	panel.add(scroll);
	
	lblEfectivo = new JLabel("Efectivo:");
	lblEfectivo.setFont(new Font("Tahoma", Font.PLAIN, 13));
	lblEfectivo.setHorizontalAlignment(SwingConstants.RIGHT);
	lblEfectivo.setBounds(271, 271, 93, 24);
	panel.add(lblEfectivo);
	
	lblPagare = new JLabel("Pagare:");
	lblPagare.setHorizontalAlignment(SwingConstants.RIGHT);
	lblPagare.setFont(new Font("Tahoma", Font.PLAIN, 13));
	lblPagare.setBounds(271, 306, 93, 24);
	panel.add(lblPagare);
	
	lblCheque = new JLabel("Cheque:");
	lblCheque.setHorizontalAlignment(SwingConstants.RIGHT);
	lblCheque.setFont(new Font("Tahoma", Font.PLAIN, 13));
	lblCheque.setBounds(271, 341, 93, 24);
	panel.add(lblCheque);
	
	lblDolares = new JLabel("Transferencia o Deposito:");
	lblDolares.setHorizontalAlignment(SwingConstants.RIGHT);
	lblDolares.setFont(new Font("Tahoma", Font.PLAIN, 13));
	lblDolares.setBounds(194, 375, 170, 24);
	panel.add(lblDolares);
	
	lblGranTotal = new JLabel("Gran Total:");
	lblGranTotal.setForeground(Color.BLUE);
	lblGranTotal.setHorizontalAlignment(SwingConstants.RIGHT);
	lblGranTotal.setFont(new Font("Tahoma", Font.BOLD, 28));
	lblGranTotal.setBounds(194, 415, 170, 24);
	panel.add(lblGranTotal);
	
	efectivoLbl = new JLabel("0.00");
	efectivoLbl.setHorizontalAlignment(SwingConstants.RIGHT);
	efectivoLbl.setFont(new Font("Tahoma", Font.BOLD, 18));
	efectivoLbl.setBounds(393, 269, 179, 24);
	panel.add(efectivoLbl);
	
	pagareLbl = new JLabel("0.00");
	pagareLbl.setHorizontalAlignment(SwingConstants.RIGHT);
	pagareLbl.setFont(new Font("Tahoma", Font.BOLD, 18));
	pagareLbl.setBounds(393, 306, 179, 24);
	panel.add(pagareLbl);
	
	chequeLbl = new JLabel("0.00");
	chequeLbl.setHorizontalAlignment(SwingConstants.RIGHT);
	chequeLbl.setFont(new Font("Tahoma", Font.BOLD, 18));
	chequeLbl.setBounds(393, 341, 179, 24);
	panel.add(chequeLbl);
	
	depositoLbl = new JLabel("0.00");
	depositoLbl.setHorizontalAlignment(SwingConstants.RIGHT);
	depositoLbl.setFont(new Font("Tahoma", Font.BOLD, 18));
	depositoLbl.setBounds(393, 375, 179, 24);
	panel.add(depositoLbl);
	
	granTotalLbl = new JLabel("0.00");
	granTotalLbl.setHorizontalAlignment(SwingConstants.RIGHT);
	granTotalLbl.setForeground(Color.BLUE);
	granTotalLbl.setFont(new Font("Tahoma", Font.BOLD, 32));
	granTotalLbl.setBounds(415, 415, 170, 30);
	panel.add(granTotalLbl);
	
	btnAceptar = new JButton("Aceptar");
	btnAceptar.setBounds(498, 493+60, 89, 23);
	btnAceptar.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			closeFrame();
		}
	});
	getContentPane().add(btnAceptar);
	
	calculateTotals();
	}
	
	private List<Cliente> checkDuplicates(List<Cliente> lista){
		List<Cliente> lista_set = new ArrayList<>();
		Cliente c0 = null;
		for(Cliente c1 : lista){
			
			if(c0==null){
				c0 = c1;
				lista_set.add(c0);
				continue;
			}
			
			if(c0.getId() == c1.getId()){
//				lista_set.add(c1);
				c0 = c1;
			}else if(c0.getId() != c1.getId()) {
				lista_set.add(c1);
				c0 = c1;
			}
		}
		return lista_set;
	}
	
	private void calculateTotals(){
		
		efectivoTotal=0.0;
		pagareTotal=0.0;
		chequeTotal=0.0;
		despositoTotal=0.0;
		granTotal=0.0;
		clientes_pedido = pedidoService.findPedidosByDate(new Date());
//		clientes_pedido = checkDuplicates(clientes_pedido);
		
		for(Venta v : ventas){
			if(v.getFormapago().getFormapago().equals("EFECTIVO")){
				efectivoTotal += v.getTotal();
			}else if(v.getFormapago().getFormapago().equals("TARJETA")){
				pagareTotal += v.getTotal();
			}else if(v.getFormapago().getFormapago().equals("CHEQUE")){
				chequeTotal += v.getTotal();
			}else if(v.getFormapago().getFormapago().equals("DEPOSITO")){
				despositoTotal += v.getTotal();
			}
		}
		
		if(((Date) datePicker.getModel().getValue()).getDate() == new Date().getDate()){
			for(Pedido p : clientes_pedido){
				if(p.getFormapago().getFormapago().equals("EFECTIVO")){
					efectivoTotal += p.getAnticipo();
				}else if(p.getFormapago().getFormapago().equals("TARJETA")){
					pagareTotal += p.getAnticipo();
				}else if(p.getFormapago().getFormapago().equals("CHEQUE")){
					chequeTotal += p.getAnticipo();
				}else if(p.getFormapago().getFormapago().equals("TRANSFERENCIA")){
					despositoTotal += p.getAnticipo();
				}
		}
		
		}
		
		efectivoLbl.setText(decimalFormat.format(efectivoTotal));
		pagareLbl.setText(decimalFormat.format(pagareTotal));
		chequeLbl.setText(decimalFormat.format(chequeTotal));
		depositoLbl.setText(decimalFormat.format(despositoTotal));
		granTotal += efectivoTotal;
		granTotal += pagareTotal;
		granTotal += chequeTotal;
		granTotal += despositoTotal;
		
		granTotalLbl.setText(decimalFormat.format(granTotal));
	}
	
	private Double calculatePedidosTotals(){
		Double total = 0.0;
		for(Venta v : pedidos){
			total += v.getTotal(); 
		}
		return total;
	}


	private void setTableData() {
		for(Venta v : ventas){
			model.insertRow(model.getRowCount(), new Object[] {  v.getNumventa(), 
																 v.getCantidad(), 
																 v.getProducto().getArticulo().getNombre()+" "+
																 v.getProducto().getMarca().getNombre()+" "+
																 v.getProducto().getDescripcion()+", "+
																 v.getProducto().getColor().getNombre()+", "+
																 v.getProducto().getTalla().getNombre(),
																 v.getDescuento(), 
																 decimalFormat.format(v.getSubtotal()), 
																 decimalFormat.format(v.getTotal()),
																 v.getFormapago().getFormapago()});
		}
	}
	
	private void closeFrame(){
		this.dispose();
	}
}
