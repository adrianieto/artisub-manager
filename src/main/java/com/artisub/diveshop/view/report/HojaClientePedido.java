package com.artisub.diveshop.view.report;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.artisub.diveshop.Referenciable;
import com.artisub.diveshop.controller.dao.ClienteDao;
import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.PedidoDao;
import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Pedido;
import com.artisub.diveshop.model.Venta;

public class HojaClientePedido extends JInternalFrame implements Referenciable<PedidosFrame>{

	private static final long serialVersionUID = -7296816615640561275L;
	private JTable table;
	private JPanel clientePanel, statusPanel, productListPanel;
	
	private Container container;
	
	private JLabel lblNombre, lblEmail, lblApellidos, lblTelefono, lblTotal, lblAnticipo, lblResto;
	private JLabel nombre, apellidos, telefono, email, total, anticipo, resto;
	private JCheckBox chckbxEntregado;
	private JButton aceptarBtn, cancelarBtn;
	
	private JScrollPane scrollPane;
	private DefaultTableModel model; 
	
	private Pedido pedido;
	private List<Venta> productos_pedido;
	
	private DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");
	
	private Font font11, font16;
	
	private double totalsum = 0.0;
	
	private IDAO<Pedido> pedidoService = new PedidoDao();
	
	private PedidosFrame pedidosFrame;
	
	/**
	* Create the frame.
	*/
	public HojaClientePedido(List<Venta> productos_pedido, Pedido pedido) {
		super("Pedido ", true, true, false,true);
		setBounds(430,100,510, 500);
//		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.pedido = pedido;
		this.productos_pedido = productos_pedido;
		init();
	}

	protected  void init(){
		container = getContentPane();
		container.setLayout(new BorderLayout());
		
		font16 = new Font("Tahoma", Font.BOLD, 16);
		font11 = new Font("Tahoma", Font.BOLD, 11);
		
		model = new DefaultTableModel();
		model.addColumn("Cant.");
		model.addColumn("Descripcion");
		model.addColumn("Total");
		
		table = new JTable(model);
		table.setPreferredScrollableViewportSize(new Dimension(290, 120));
		table.setFillsViewportHeight(true);
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 11));
		
		table.getColumnModel().getColumn(0).setPreferredWidth(10);
		table.getColumnModel().getColumn(1).setPreferredWidth(250);
		table.getColumnModel().getColumn(2).setPreferredWidth(50);
		
//		table.setShowGrid(false);
	    table.setShowVerticalLines(true);
	    table.addMouseListener(new MouseAdapter() {
	    	public void mouseClicked(MouseEvent e){
	    		if(e.getClickCount() == 2){
	    				table.getSelectionModel().clearSelection();
	    		}
	    	}
		});
		

	    TableCellRenderer rendererFromHeader = table.getTableHeader().getDefaultRenderer();
	    JLabel headerLabel = (JLabel) rendererFromHeader;
	    headerLabel.setHorizontalAlignment(JLabel.CENTER);
		
		
		scrollPane = new JScrollPane(table);
		
		
		
		productListPanel = new JPanel();
		productListPanel.setBackground(SystemColor.control);
		productListPanel.setLayout(new BoxLayout(productListPanel, BoxLayout.Y_AXIS));
//		formasdepagoPanel.setBorder(new TitledBorder(null, paneltitle, TitledBorder.LEADING, TitledBorder.TOP, null, Color.DARK_GRAY));
		productListPanel.setBorder(BorderFactory.createTitledBorder("Productos"));
	
		
		productListPanel.add(scrollPane);
		
		
		clientePanel = new JPanel(new FlowLayout());
		clientePanel.setBackground(Color.WHITE);
		clientePanel.setLayout(null);
		clientePanel.setPreferredSize(new Dimension(430, 100));
		clientePanel.setBorder(BorderFactory.createTitledBorder("Datos del Cliente "));
		
		
		lblNombre = new JLabel("Nombre: ");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNombre.setBounds(20, 28, 50, 14);
		clientePanel.add(lblNombre);
		nombre = new JLabel(pedido.getCliente().getNombre());
		nombre.setFont(new Font("Tahoma", Font.BOLD, 11));
		nombre.setForeground(Color.BLUE);
		nombre.setBounds(75, 28, 60, 14);
		clientePanel.add(nombre);
		
		lblEmail = new JLabel("Email:   ");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmail.setBounds(222, 63, 62, 14);
		clientePanel.add(lblEmail);
		email = new JLabel(pedido.getCliente().getEmail());
		email.setFont(new Font("Tahoma", Font.BOLD, 11));
		email.setBounds(222+65, 63, 200, 14);
		email.setForeground(Color.BLUE);
		clientePanel.add(email);
		
		lblApellidos = new JLabel("Apellidos: ");
		lblApellidos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblApellidos.setBounds(222, 28, 62, 14);
		clientePanel.add(lblApellidos);
		apellidos = new JLabel(pedido.getCliente().getApellidos());
		apellidos.setFont(new Font("Tahoma", Font.BOLD, 11));
		apellidos.setBounds(222+65, 28, 150, 14);
		apellidos.setForeground(Color.BLUE);
		clientePanel.add(apellidos);
		
		lblTelefono = new JLabel("Tel: ");
		lblTelefono.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTelefono.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTelefono.setBounds(20, 63, 50, 14);
		clientePanel.add(lblTelefono);
		telefono = new JLabel(pedido.getCliente().getTelefono());
		telefono.setFont(new Font("Tahoma", Font.BOLD, 11));
		telefono.setBounds(75, 63, 120, 14);
		telefono.setForeground(Color.BLUE);
		clientePanel.add(telefono);

		statusPanel = new JPanel();
		statusPanel.setBackground(SystemColor.control);
		statusPanel.setPreferredSize(new Dimension(430, 150));
		statusPanel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		statusPanel.setLayout(null);
		
		chckbxEntregado = new JCheckBox("Entregado");
		chckbxEntregado.setFont(font16);
		chckbxEntregado.setForeground(Color.DARK_GRAY);
		chckbxEntregado.setBounds(35, 35, 200, 20);
		statusPanel.add(chckbxEntregado);
		
		lblTotal = new JLabel("Total: $ ");
		lblTotal.setFont(font16);
		lblTotal.setForeground(Color.DARK_GRAY);
		lblTotal.setBounds(250, 10, 100, 20);
		lblTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		statusPanel.add(lblTotal);
		
		lblAnticipo = new JLabel("Anticipo: $ ");
		lblAnticipo.setFont(font16);
		lblAnticipo.setBounds(250,35,100,20);
		lblAnticipo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAnticipo.setForeground(Color.DARK_GRAY);
		statusPanel.add(lblAnticipo);
		
		lblResto = new JLabel("Restan: $ ");
		lblResto.setFont(font16);
		lblResto.setBounds(250, 60, 100,20);
		lblResto.setHorizontalAlignment(SwingConstants.RIGHT);
		lblResto.setForeground(Color.DARK_GRAY);
		statusPanel.add(lblResto);
		
		total = new JLabel("0.00");
		total.setFont(font16);
		total.setBounds(310, 10, 150, 20);
		total.setHorizontalAlignment(SwingConstants.RIGHT);
		total.setForeground(Color.BLACK);
		statusPanel.add(total);
		
		anticipo = new JLabel("0.00");
		anticipo.setFont(font16);
		anticipo.setBounds(310, 35, 150, 20);
		anticipo.setHorizontalAlignment(SwingConstants.RIGHT);
		anticipo.setForeground(Color.BLUE);
		statusPanel.add(anticipo);
		
		resto = new JLabel("0.00");
		resto.setFont(font16);
		resto.setBounds(310, 60, 150, 20);
		resto.setHorizontalAlignment(SwingConstants.RIGHT);
		resto.setForeground(Color.RED);
		statusPanel.add(resto);
		
		aceptarBtn = new JButton("Aceptar");
		aceptarBtn.setFont(font11);
		aceptarBtn.setBounds(250, 100, 100, 35);
		aceptarBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				checkDelivery();
			}
		});
		statusPanel.add(aceptarBtn);
		
		cancelarBtn = new JButton("Cancelar");
		cancelarBtn.setFont(font11);
		cancelarBtn.setBounds(380, 100, 100, 35);
		cancelarBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
				
			}
		});
		statusPanel.add(cancelarBtn);
		
		setTableData(productos_pedido);
		
		container.add(clientePanel, BorderLayout.PAGE_START);
//		container.add(menuPanel, BorderLayout.LINE_START);
		
		container.add(productListPanel, BorderLayout.CENTER);
		container.add(statusPanel, BorderLayout.PAGE_END);
		
	}
	private void setTableData(List<Venta> productos){
		for(Venta v : productos){
			model.insertRow(model.getRowCount(), new Object[] {  
				v.getCantidad(),
				v.getProducto().getArticulo().getNombre()+"  "+
				v.getProducto().getMarca().getNombre()+"  "+
				v.getProducto().getDescripcion()+"  "+
				v.getProducto().getColor().getNombre()+"  "+
				v.getProducto().getTalla().getNombre(),
				"$ "+decimalFormat.format(v.getTotal()*v.getCantidad())
			});
			totalsum += v.getTotal();
		}
		total.setText(decimalFormat.format(totalsum));
		anticipo.setText(decimalFormat.format(pedido.getAnticipo().doubleValue()));
		resto.setText(decimalFormat.format(totalsum - pedido.getAnticipo().doubleValue()));
		chckbxEntregado.setSelected(pedido.getPedidoEntregado());
	}
	
	private void checkDelivery(){
		if(chckbxEntregado.isSelected()){
			pedido.setPedidoEntregado(true);
			pedidoService.update(pedido);
			pedidosFrame.model.setValueAt("ENTREGADO", pedidosFrame.table.getSelectedRow(), 3);
			close();
		}else{
			pedido.setPedidoEntregado(false);
			pedidoService.update(pedido);
			pedidosFrame.model.setValueAt("PENDIENTE", pedidosFrame.table.getSelectedRow(), 3);
			pedidosFrame.table.repaint();
			close();
		}
	}
	
	private void close(){
		this.dispose();
	}
	

	@Override
	public void setReference(PedidosFrame t) {
		pedidosFrame=t;
	}

}
