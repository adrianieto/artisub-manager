package com.artisub.diveshop.view.report;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.artisub.diveshop.controller.dao.PedidoDao;
import com.artisub.diveshop.controller.dao.VentaDao;
import com.artisub.diveshop.model.Pedido;
import com.artisub.diveshop.model.Venta;

public class PedidosFrame extends JInternalFrame {
	
	private static final long serialVersionUID = 6648391006770554025L;
	
	protected JTable table;
	private JScrollPane scroll;
	private JPanel panel;
	protected DefaultTableModel model;
	private JButton btnCancelar, btnVerPedido;
	
	//Persistance service
	private  PedidoDao pedidoService= new PedidoDao();
	private List<Pedido> pedidos = pedidoService.findAll();
	
	private VentaDao ventaService = new VentaDao();
	
//	private VentaDao ventaService = new VentaDao();
//	private List<Object[]> ventasClientePedido = null;
	private List<Pedido> ventasPedido = new ArrayList<Pedido>();
//	private DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd / MM / yyyy");
	
	/**
	* Create the frame.
	*/
	public PedidosFrame() {
		super("Pedidos",false,true,false,false);
		setBounds(345, 40, 634+40, 569);
		getContentPane().setLayout(null);
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Hoja de Pedidos", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 595+44, 471);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		model = new DefaultTableModel();
		model.addColumn("Venta #");
		model.addColumn("Nombre");
		model.addColumn("Fecha");
		model.addColumn("Status");
		
		UIDefaults defaults = UIManager.getLookAndFeelDefaults();
		if (defaults.get("Table.alternateRowColor") == null)
		    defaults.put("Table.alternateRowColor", new Color(235, 235, 240));
		
		table = new JTable(model);
		table.setShowGrid(false);
	    table.setShowVerticalLines(true);
		table.setBounds(10, 27, 575+45, 340);
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	    centerRenderer.setHorizontalAlignment( JLabel.CENTER );
	    
		table.getColumnModel().getColumn(0).setPreferredWidth(20);
		table.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(1).setPreferredWidth(240);
		table.getColumnModel().getColumn(2).setPreferredWidth(50);
		table.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(3).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		
		setTableData();
		scroll = new JScrollPane(table);
		scroll.setBounds(10, 27, 575+45, 340);
		panel.setBackground(Color.WHITE);
		panel.add(scroll);
	
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(460, 390, 89, 33);
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
				
			}
		});
		panel.add(btnCancelar);
		
		btnVerPedido = new JButton("Ver Pedido");
		btnVerPedido.setBounds(50, 390, 100, 33);
		btnVerPedido.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showPedido();
			}
		});
		panel.add(btnVerPedido);
	}
	
	private void showPedido(){	
		Pedido pedido = ventasPedido.get(table.getSelectedRow());
		List<Venta> articulos_pedidos = new ArrayList<Venta>(); 
			for(Venta v : ventaService.findByNumVenta(pedido.getNumVenta())){
				articulos_pedidos.add(v);	
			}
			
		HojaClientePedido hojaPedido = new HojaClientePedido(articulos_pedidos,pedido);
		hojaPedido.setReference(this);
		hojaPedido.setVisible(true);
		com.artisub.diveshop.view.MainFrame.desktopPane.add(hojaPedido, JDesktopPane.POPUP_LAYER);
		try{
			hojaPedido.setSelected(true);
		}catch(PropertyVetoException e1){
			e1.printStackTrace();
		}
	}

	protected void setTableData() {
		int numventa=0;
		for(Pedido p : pedidos){	
			if(numventa==0 || numventa!=p.getNumVenta()){
				ventasPedido.add(p);
				model.insertRow(model.getRowCount(), new Object[] {  
					p.getNumVenta(), 
					p.getCliente().getNombre()+" "+p.getCliente().getApellidos(),
					dateFormat.format(p.getFecha()),
//					"$ "+decimalFormat.format(v.getTotal())
					(p.getPedidoEntregado())?"ENTREGADO":"PENDIENTE"
				});
			}else{
				continue;
			}
				
			numventa = p.getNumVenta();
		}
		
	}
	
	private void close(){
		this.dispose();
	}
}
