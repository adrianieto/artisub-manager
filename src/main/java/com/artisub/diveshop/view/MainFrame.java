package com.artisub.diveshop.view;

import com.artisub.diveshop.view.catalogos.*;
import com.artisub.diveshop.view.pos.CancelaVenta;
import com.artisub.diveshop.view.pos.PuntoDeVenta;
import com.artisub.diveshop.view.pos.PuntoDeVentaPedido;
import com.artisub.diveshop.view.report.CorteDeCajaFrame;
import com.artisub.diveshop.view.report.MarketshareFrame;
import com.artisub.diveshop.view.report.PedidosFrame;
import com.artisub.diveshop.view.report.ReporteMes;
import com.sun.glass.events.KeyEvent;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -3360344585722141592L;

	private JPanel contentPane;

	public static JDesktopPane desktopPane;
	private JMenuBar menuBar;
	private JMenu catalogosMenu,puntoDeVentaMenu, reportesMenu;
	private JMenuItem productosMenuItem, tallasMenuItem, articulosMenuItem,marcasMenuItem,formasDePagoMenuItem,
					cancelarVentaMenuItem, clientesMenuItem;
	private JMenuItem puntoDeVentaMenuItem, puntoDeVentaPedidoMenuItem;
	private JMenuItem corteDeCajaMenuItem, pedidosMenuItem, reporteMesMenuItem,marketShareMenuItem;
	private JSeparator separator;
//	private JButton pventa;
	private Font font = new Font("Lao UI",Font.PLAIN, 13);
	public String windowName = "Punto de Venta";
	private MainFrame mainframe = this;
	JLabel background = new JLabel();
	 private BufferedImage img;
	/**
	 * Create the frame.
	 */
	@SuppressWarnings("serial")
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Artisub Sales Manager   v 0.1.6 alpha"); 
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(0, 0,
                  screenSize.width,
                  screenSize.height-40);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
//		background.
		
		try {
            img = ImageIO.read(new File("src/main/resources/wallpaper_artisub.jpg"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
		
		desktopPane = new JDesktopPane(){
			 @Override
	            protected void paintComponent(Graphics grphcs) {
	                super.paintComponent(grphcs);
	                grphcs.drawImage(img,0,0, null);
	                
	            }

	            @Override
	            public Dimension getPreferredSize() {
	                return new Dimension(img.getWidth(), img.getHeight());
	            }
		};
		desktopPane.setBackground(SystemColor.controlHighlight);
//		desktopPane.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		
		contentPane.add(desktopPane, BorderLayout.CENTER);
		
		createMenu();
//		pventa = new JButton("Punto de Venta");
//		pventa.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				PuntoDeVenta puntoDeVenta = new PuntoDeVenta();
//				puntoDeVenta.setVisible(true);
//				desktopPane.add(puntoDeVenta);
//			}
//		});
//		desktopPane.add(pventa);
		
		setContentPane(desktopPane);
	}
	
	private void createMenu(){
		menuBar = new JMenuBar();
		
		setJMenuBar(menuBar);
		
		
		puntoDeVentaMenu = new JMenu("Caja");
		puntoDeVentaMenu.setFont(font);
		menuBar.add(puntoDeVentaMenu);
		
		catalogosMenu = new JMenu("Inventario");
		catalogosMenu.setMnemonic('I');
		catalogosMenu.setFont(font);
		menuBar.add(catalogosMenu);
		
		reportesMenu = new JMenu("Reportes");
		reportesMenu.setFont(font);
		menuBar.add(reportesMenu);
		
		pedidosMenuItem = new JMenuItem("Pedidos");
		pedidosMenuItem.setFont(font);
		pedidosMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread corteCaja = new Thread(new Runnable() {
					@Override
					public void run() {
						PedidosFrame pedidos = new PedidosFrame();
						pedidos.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						pedidos.setVisible(true);
						
						desktopPane.add(pedidos, JDesktopPane.POPUP_LAYER);
						try{
							pedidos.setSelected(true);
						}catch (PropertyVetoException e1){
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				corteCaja.start();
			}
		});
		reportesMenu.add(pedidosMenuItem);
		
		

		
		corteDeCajaMenuItem = new JMenuItem("Resumen de Ventas");
		corteDeCajaMenuItem.setFont(font);
		corteDeCajaMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread corteCaja = new Thread(new Runnable() {
					@Override
					public void run() {
						CorteDeCajaFrame corte = new CorteDeCajaFrame();
						corte.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						corte.setVisible(true);
						
						desktopPane.add(corte, JDesktopPane.POPUP_LAYER);
						try{
							corte.setSelected(true);
						}catch (PropertyVetoException e1){
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				corteCaja.start();
				
				
			}
		});
		
		reportesMenu.add(corteDeCajaMenuItem);
		
		reporteMesMenuItem = new JMenuItem("Reporte del Mes");
		reporteMesMenuItem.setFont(font);
		reporteMesMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread reportemes = new Thread(new Runnable() {
					@Override
					public void run() {
						ReporteMes rmes = new ReporteMes();
						rmes.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						rmes.setVisible(true);
						desktopPane.add(rmes, JDesktopPane.POPUP_LAYER);
						try{
							rmes.setSelected(true);
						}catch (PropertyVetoException e1){
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				reportemes.start();
			}
		});
		reportesMenu.add(reporteMesMenuItem);
		
		marketShareMenuItem = new JMenuItem("Marketshare");
		marketShareMenuItem.setFont(font);
		marketShareMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread reportemarket = new Thread(new Runnable() {
					@Override
					public void run() {
						MarketshareFrame marketFrame = new MarketshareFrame();
						marketFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						marketFrame.setVisible(true);
						desktopPane.add(marketFrame, JDesktopPane.POPUP_LAYER);
						try{
							marketFrame.setSelected(true);
						}catch (PropertyVetoException e1){
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				reportemarket.start();
			}	
		});
		reportesMenu.add(marketShareMenuItem);
		
		puntoDeVentaMenuItem = new JMenuItem("Punto de Venta");
		puntoDeVentaMenuItem.setFont(font);
		puntoDeVentaMenuItem.setMnemonic(KeyEvent.VK_P);
		puntoDeVentaMenuItem.setAccelerator(KeyStroke.getKeyStroke("control P"));
		puntoDeVentaMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Cambia el cusor a cursor de espera hasta que la ejecuacion del hijo temina y
				//regresa el estado del cursor a default
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				//Declaracion del Hilo
				Thread pventaThread = new Thread(new Runnable() {
					@Override
					public void run() {
						PuntoDeVenta pv = new PuntoDeVenta();
						pv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						pv.setVisible(true);
						
						desktopPane.add(pv,JDesktopPane.POPUP_LAYER);
						try {
							pv.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				//Inicializacion del Hilo
				pventaThread.start();
				
			}
		});
		puntoDeVentaMenu.add(puntoDeVentaMenuItem);
		
		puntoDeVentaPedidoMenuItem = new JMenuItem("Venta Sobre Pedido");
		puntoDeVentaPedidoMenuItem.setFont(font);
		puntoDeVentaPedidoMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Cambia el cusor a cursor de espera hasta que la ejecuacion del hijo temina y
				//regresa el estado del cursor a default
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				//Declaracion del Hilo
				Thread pventaThread = new Thread(new Runnable() {
					@Override
					public void run() {
						PuntoDeVentaPedido pv = new PuntoDeVentaPedido();
						pv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						pv.setVisible(true);
						
						desktopPane.add(pv,JDesktopPane.POPUP_LAYER);
						try {
							pv.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				//Inicializacion del Hilo
				pventaThread.start();
				
			}
		});
		puntoDeVentaMenu.add(puntoDeVentaPedidoMenuItem);
		
		cancelarVentaMenuItem = new JMenuItem("Cancelar Venta");
		cancelarVentaMenuItem.setFont(font);
		cancelarVentaMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Cambia el cusor a cursor de espera hasta que la ejecuacion del hijo temina y
				//regresa el estado del cursor a default
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				//Declaracion del Hilo
				Thread pventaThread = new Thread(new Runnable() {
					@Override
					public void run() {
						CancelaVenta cv = new CancelaVenta();
						cv.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						cv.setVisible(true);
						
						desktopPane.add(cv,JDesktopPane.POPUP_LAYER);
						try {
							cv.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				//Inicializacion del Hilo
				pventaThread.start();
				
			}
		});
		puntoDeVentaMenu.add(cancelarVentaMenuItem);
		
		
		productosMenuItem = new JMenuItem("Productos");
		productosMenuItem.setFont(font);
		productosMenuItem.setMnemonic(KeyEvent.VK_I);
		productosMenuItem.setAccelerator(KeyStroke.getKeyStroke("control I"));
		productosMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread productoThread = new Thread(new Runnable() {
					@Override
					public void run() {
						ProductoCatalogo pc = new ProductoCatalogo();
						pc.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						pc.setVisible(true);
						desktopPane.add(pc,JDesktopPane.POPUP_LAYER);
						try {
							pc.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				productoThread.start();
			}
		});
		catalogosMenu.add(productosMenuItem);
		
		separator = new JSeparator();
		catalogosMenu.add(separator);
		
		clientesMenuItem = new JMenuItem("Catalogo de Clientes");
		clientesMenuItem.setFont(font);
		clientesMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread clientesThread = new Thread(new Runnable() {
					@Override
					public void run() {
						ClientesCatalogo tc = new ClientesCatalogo("Catalogo de Clientes","Catalogo de Clientes");
						tc.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						tc.setVisible(true);
						desktopPane.add(tc,JDesktopPane.POPUP_LAYER);
						try {
							tc.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				clientesThread.start();
			}
		});
		catalogosMenu.add(clientesMenuItem);
		
		tallasMenuItem = new JMenuItem("Catalogo de Tallas");
		tallasMenuItem.setFont(font);
		tallasMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread tallasThread = new Thread(new Runnable() {
					@Override
					public void run() {
						TallaCatalogo tc = new TallaCatalogo();
						tc.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						tc.setVisible(true);
						desktopPane.add(tc,JDesktopPane.POPUP_LAYER);
						try {
							tc.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				tallasThread.start();
			}
		});
		catalogosMenu.add(tallasMenuItem);
		
		JMenuItem mntmColores = new JMenuItem("Catalogo de Colores");
		mntmColores.setFont(font);
		mntmColores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread coloresThread = new Thread(new Runnable() {
					@Override
					public void run() {
						ColorCatalogo cc= new ColorCatalogo();
						cc.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						cc.setVisible(true);
						desktopPane.add(cc,JDesktopPane.POPUP_LAYER);
						try {
							cc.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				coloresThread.start();
			}
		});
		catalogosMenu.add(mntmColores);
		
		articulosMenuItem = new JMenuItem("Catalogo de Articulos");
		articulosMenuItem.setFont(font);
		articulosMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread articulosThread = new Thread(new Runnable() {
					@Override
					public void run() {
						ArticuloCatalogo ac = new ArticuloCatalogo();
						ac.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						ac.setVisible(true);
						desktopPane.add(ac,JDesktopPane.POPUP_LAYER);
						try {
							ac.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				articulosThread.start();
			}
		});
		catalogosMenu.add(articulosMenuItem);
		
		marcasMenuItem = new JMenuItem("Catalogo de Marcas");
		marcasMenuItem.setFont(font);
		marcasMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread marcasThread = new Thread(new Runnable() {
					@Override
					public void run() {
						MarcaCatalogo mc = new MarcaCatalogo();
						mc.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						mc.setVisible(true);
						desktopPane.add(mc,JDesktopPane.POPUP_LAYER);
						try {
							mc.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				marcasThread.start();
			}
		});
		catalogosMenu.add(marcasMenuItem);
		
		formasDePagoMenuItem = new JMenuItem("Formas de Pago");
		formasDePagoMenuItem.setFont(font);
		formasDePagoMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				desktopPane.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Thread fpagoThread = new Thread(new Runnable() {
					@Override
					public void run() {
						FormaPagoCatalogo fpc= new FormaPagoCatalogo();
						fpc.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						fpc.setVisible(true);
						desktopPane.add(fpc,JDesktopPane.POPUP_LAYER);
						try {
							fpc.setSelected(true);
						} catch (PropertyVetoException e1) {
							e1.printStackTrace();
						}
						desktopPane.setCursor(Cursor.getDefaultCursor());
					}
				});
				fpagoThread.start();
			}
		});
		catalogosMenu.add(formasDePagoMenuItem);
	}

}
