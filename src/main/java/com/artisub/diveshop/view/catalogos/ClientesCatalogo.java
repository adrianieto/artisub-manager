package com.artisub.diveshop.view.catalogos;

import java.beans.PropertyVetoException;
import java.util.List;

import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.artisub.diveshop.controller.dao.ClienteDao;
import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Producto;
import com.artisub.diveshop.view.MainFrame;

public class ClientesCatalogo extends Catalogo {
	
	TableRowSorter<TableModel> sorter;
	public DefaultTableModel modelo = new DefaultTableModel();
	
	private IDAO<Cliente> clienteService = new ClienteDao();
	public List<Cliente> lista_clientes = clienteService.findAll();

	public ClientesCatalogo(String titleframe, String paneltitle) {
		super(titleframe,paneltitle);
		setSize(780, 475);
		setParentsVars();
		setTableData();
	}
	
	private void setParentsVars(){
		modelo.addColumn("ID");
		modelo.addColumn("Nombre");
		modelo.addColumn("Apellidos");
		modelo.addColumn("Telefono");
		modelo.addColumn("Email");
		
		sorter = new TableRowSorter<TableModel>(modelo);
		table.setModel(modelo);
	    table.setRowSorter(sorter);
	    
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( SwingConstants.CENTER );
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(0).setPreferredWidth(10);
		table.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(1).setPreferredWidth(30);
		table.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(2).setPreferredWidth(30);
		table.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(3).setPreferredWidth(20);
		table.getColumnModel().getColumn(4).setCellRenderer( centerRenderer );
		table.getColumnModel().getColumn(4).setPreferredWidth(20);
//		setTableData();
	}

	@Override
	protected void addItem() {
		NuevoClienteDialog ncd = new NuevoClienteDialog();
		ncd.setVisible(true);
		ncd.setReference(this);
		MainFrame.desktopPane.add(ncd,JDesktopPane.POPUP_LAYER);
		try {
			ncd.setSelected(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}

	}

	@Override
	protected void editItem() {
		EditClienteDialogo editCliente = new EditClienteDialogo();
		editCliente.setVisible(true);
		editCliente.setReference(this);
		editCliente.setTextFieldVals();
		MainFrame.desktopPane.add(editCliente,JDesktopPane.POPUP_LAYER);
		try {
			editCliente.setSelected(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	protected void deleteItem() {
		Cliente cliente = clienteService.findById((int)(table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0)));		
		int ans = JOptionPane.showConfirmDialog(this, "Esta seguro de borrar al cliente: \""+cliente.getNombre()+" "+cliente.getApellidos()+"\" ?");
		if(JOptionPane.OK_OPTION == ans){
			clienteService.delete(cliente);
			lista_clientes.remove(cliente);
			modelo.removeRow(lista_clientes.indexOf(cliente)+1);
		}
		table.repaint();
	}

	@Override
	protected void setTableData() {
		for(Cliente c : lista_clientes){
			modelo.insertRow(modelo.getRowCount(), new Object[] {c.getId(),
																 c.getNombre(),
																 c.getApellidos(),
																 c.getTelefono(),
																 c.getEmail()
																 });
		}

	}

}
