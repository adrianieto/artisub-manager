package com.artisub.diveshop.view.catalogos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.artisub.diveshop.model.Venta;
import com.artisub.diveshop.view.pos.PuntoDeVenta;

public class EditListaVentaItemDialog extends JInternalFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JTextField cantidadTextField;
	private JTextField descuentoTextField;

	private PuntoDeVenta pv;
	
	private String panelTitle1, panelTitle2;
	/**
	 * Create the frame.
	 */
	public EditListaVentaItemDialog(PuntoDeVenta pv) {
		super("Editar Articulo",true,true,true,true);
		this.pv=pv;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(550, 200, 249, 204);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelTitle1  = pv.getLista_ventas().get(pv.table.getSelectedRow()).getProducto().getDescripcion();
		panelTitle2  = pv.getLista_ventas().get(pv.table.getSelectedRow()).getProducto().getArticulo().toString();
		contentPane.setBorder(BorderFactory.createTitledBorder(panelTitle2+" "+panelTitle1));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JLabel lblCantidad = new JLabel("Cantidad:");
		lblCantidad.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCantidad.setBounds(10, 23, 85, 14);
		contentPane.add(lblCantidad);
		JLabel lblDescuento = new JLabel("Descuento %:");
		lblDescuento.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDescuento.setBounds(10, 52, 85, 14);
		contentPane.add(lblDescuento);
		cantidadTextField = new JTextField();
		cantidadTextField.setBounds(118, 18, 109, 28);
		contentPane.add(cantidadTextField);
		cantidadTextField.setColumns(10);
		descuentoTextField = new JTextField();
		descuentoTextField.setBounds(118, 46, 109, 28);
		contentPane.add(descuentoTextField);
		descuentoTextField.setColumns(10);
		
		getSelectedTableValues();
		
		JButton btnNewButton = new JButton("Aceptar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setTableValues();
				closeFrame();
			}
		});
		btnNewButton.setBounds(134, 102, 89, 23);
		contentPane.add(btnNewButton);
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(35, 102, 89, 23);
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeFrame();
				
			}
		});
		contentPane.add(btnCancelar);
	}

	public void closeFrame(){
		this.dispose();
	}
	
	private void getSelectedTableValues(){
		cantidadTextField.setText(pv.getLista_ventas().get(pv.table.getSelectedRow()).getCantidad().toString());
		descuentoTextField.setText(pv.getLista_ventas().get(pv.table.getSelectedRow()).getDescuento().toString());
	}
	
	private void setTableValues(){
		Venta v = pv.getLista_ventas().get(pv.table.getSelectedRow());
		
		double precio = Double.parseDouble(v.getProducto().getPrecio().toString());
		precio = precio * com.artisub.diveshop.App.TIPO_DE_CAMBIO;
		int cantidad = Integer.parseInt(cantidadTextField.getText());
		double descuento = Double.parseDouble(descuentoTextField.getText());
		
		int index = pv.getLista_ventas().indexOf(v);
		v.setCantidad(Short.parseShort(cantidadTextField.getText()));
		v.setDescuento(Double.parseDouble(descuentoTextField.getText()));
		//Inserta en la misma psoiscion de la lsita el objeto con los cambios
		pv.getLista_ventas().set(index, v);
		//cambia los valores mostrados en JTable de ventas
		pv.model.setValueAt(v.getCantidad(), index, 5);
		pv.model.setValueAt(v.getDescuento(), index, 6);
		pv.model.setValueAt(precio ,index, 7);
		pv.model.setValueAt(precio*cantidad, index, 8);
		//Actualiza el JTable para mostrar los cambios en la lista de venta
		pv.table.repaint();
		
		if(cantidad<v.getCantidad() && cantidad>0){
			pv.subTotalAC -= precio;
		}else{
			pv.subTotalAC += precio;
		}
		if(descuento < v.getDescuento()){
			pv.descuento -= (descuento/100)*precio;
		}else{
			pv.descuento += (descuento/100)*precio;
		}
		pv.descuentoLbl.setText(pv.decimalFormat.format(pv.descuento));
		pv.subTotalLbl.setText(pv.decimalFormat.format(pv.subTotalAC));
		Double total = pv.subTotalAC-pv.descuento;
		pv.totalLbl.setText(pv.decimalFormat.format(total));
	}
}
