package com.artisub.diveshop.view.catalogos;

import com.artisub.diveshop.Referenciable;
import com.artisub.diveshop.model.Cliente;

public class EditClienteDialogo extends ClienteDialog implements Referenciable<ClientesCatalogo> {

	private static final long serialVersionUID = 5745531787259276787L;
	private ClientesCatalogo clientesCatalogo;
	
	public EditClienteDialogo() {
		 
	}
	
	protected void setTextFieldVals(){
		int selectedRow = clientesCatalogo.table.getSelectedRow();
		nombre.setText((String)clientesCatalogo.modelo.getValueAt(selectedRow, 1));
		apellidos.setText((String)clientesCatalogo.modelo.getValueAt(selectedRow, 2));
		telefono.setText((String)clientesCatalogo.modelo.getValueAt(selectedRow, 3));
		email.setText((String)clientesCatalogo.modelo.getValueAt(selectedRow, 4));
	}
	
	@Override
	public void setReference(ClientesCatalogo t) {
		clientesCatalogo = t;

	}

	@Override
	public void persistCliente() {
		Cliente cl = clienteService.findById((int)clientesCatalogo.table.getModel().getValueAt(clientesCatalogo.table.convertRowIndexToModel(clientesCatalogo.table.getSelectedRow()), 0));
		cl.setNombre(nombre.getText());
		cl.setApellidos(apellidos.getText());
		cl.setTelefono(telefono.getText());
		cl.setEmail(email.getText());
		
		clienteService.update(cl);
		
		int selectedRow =  clientesCatalogo.table.getSelectedRow();
		clientesCatalogo.modelo.setValueAt(cl.getId(),selectedRow, 0);
		clientesCatalogo.modelo.setValueAt(cl.getNombre(),selectedRow, 1);
		clientesCatalogo.modelo.setValueAt(cl.getApellidos(),selectedRow, 2);
		clientesCatalogo.modelo.setValueAt(cl.getTelefono(),selectedRow, 3);
		clientesCatalogo.modelo.setValueAt(cl.getEmail(),selectedRow, 4);
		
		clientesCatalogo.table.repaint();
	}

}
