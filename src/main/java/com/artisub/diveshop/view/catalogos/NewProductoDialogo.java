package com.artisub.diveshop.view.catalogos;

import com.artisub.diveshop.model.Producto;

public class NewProductoDialogo extends ProductoDialogo {

	private static final long serialVersionUID = 3752640680554238955L;
	
	private ProductoCatalogo productoCatalogo;
	
	public NewProductoDialogo(ProductoCatalogo productoCatalogo) {
		this.productoCatalogo = productoCatalogo;
//		setLocationRelativeTo(null);
		
		if(productoCatalogo.table.getSelectedRow() != -1){
			Producto producto = productoCatalogo.productoService.findById((int)(productoCatalogo.table.getModel().getValueAt(productoCatalogo.table.convertRowIndexToModel(productoCatalogo.table.getSelectedRow()), 0)));		
			int i = productoCatalogo.getMapIndex(producto.getId());
			
			marcaComboBox.setSelectedItem(productoCatalogo.modelo.getValueAt(i, 2));
			articuloComboBox.setSelectedItem(productoCatalogo.modelo.getValueAt(i, 3));
			colorComboBox.setSelectedItem(productoCatalogo.modelo.getValueAt(i, 5));
			tallaComboBox.setSelectedItem(productoCatalogo.modelo.getValueAt(i, 6));
			partetextField.setText((String)productoCatalogo.modelo.getValueAt(i, 1));
			modeloTextField.setText((String)productoCatalogo.modelo.getValueAt(i, 4));
			precioTextField.setText(productoCatalogo.modelo.getValueAt(i, 8).toString());
			existenciasTextField.setText(productoCatalogo.modelo.getValueAt(i, 7).toString());
			codigoTextField.setText(producto.getCodigo());
		}
		
		
	}

	@Override
	protected void saveProducto() {
		Producto p = new Producto();
		p.setArticulo(getSelectedArticulo());
		p.setMarca(getSelectedMarca());
		p.setTalla(getSelectedTalla());
		p.setColor(getSelectedColor());
		p.setDescripcion(modeloTextField.getText().toUpperCase());
		p.setNumparte(partetextField.getText().toUpperCase());
		p.setPrecio(new Double(precioTextField.getText()));
		p.setExistencias(Integer.parseInt(existenciasTextField.getText()));
		p.setCodigo(codigoTextField.getText());
		p.setProd_no_listado(false);
		
		productoCatalogo.productoService.create(p); // persist the new object
		productoCatalogo.lista_productos.add(p);
		productoCatalogo.modelo.insertRow(productoCatalogo.modelo.getRowCount(), new Object[]{p.getId(),
																							  p.getNumparte(), 
																							  p.getMarca().getNombre(), 
																							  p.getArticulo().getNombre(), 
																							  p.getDescripcion(), 
																							  p.getColor().getNombre(), 
																							  p.getTalla().getNombre(), 
																							  p.getExistencias(), 
																							  p.getPrecio(),
																							  p.getPrecio()*com.artisub.diveshop.App.TIPO_DE_CAMBIO
																							  });
	
		ProductoCatalogo.posindex.clear();
		int i =0;
		for(Producto prod : productoCatalogo.lista_productos){
			ProductoCatalogo.posindex.put(prod.getId(), i);
			i++;
		}
	}	

}
