package com.artisub.diveshop.view.catalogos;

import com.artisub.diveshop.Referenciable;
import com.artisub.diveshop.model.Cliente;

public class NuevoClienteDialog extends ClienteDialog implements Referenciable<ClientesCatalogo>{

	private static final long serialVersionUID = 5922294506405192105L;
	
	private ClientesCatalogo clientesCatalogo;
	
	public NuevoClienteDialog() {
		
	}
	
	
	public void persistCliente(){
		Cliente cliente = new Cliente();
		cliente.setNombre(nombre.getText());
		cliente.setApellidos(apellidos.getText());
		cliente.setTelefono(telefono.getText());
		cliente.setEmail(email.getText());
		
		clienteService.create(cliente);
		clientesCatalogo.lista_clientes.add(cliente);
		clientesCatalogo.modelo.insertRow(clientesCatalogo.modelo.getRowCount(), new Object[]{cliente.getId(),
																							  cliente.getNombre(),
																							  cliente.getApellidos(),
																							  cliente.getTelefono(),
																							  cliente.getEmail()
																							  });
	
	}

	@Override
	public void setReference(ClientesCatalogo t) {
		clientesCatalogo = t;
	}

}
