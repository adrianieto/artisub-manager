package com.artisub.diveshop.view.catalogos;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import com.artisub.diveshop.controller.dao.ClienteDao;
import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.model.Cliente;

public abstract class ClienteDialog extends JInternalFrame  {

	private static final long serialVersionUID = 1194706245608612367L;

	private Font font = new Font("Lao UI", Font.BOLD, 13);
	
	private ClientesCatalogo clientesCatalogo;
	private Cliente cliente;
	
	private Container container;
	private JPanel panel;
	private JLabel lblNombre, lblApellidos, lblTelefono, lblEmail;
	public JTextField nombre, apellidos, telefono, email;
	private JButton aceptar, cancelar;
	
	public IDAO<Cliente> clienteService = new ClienteDao();
	
	public ClienteDialog() {
		super("Nuevo Cliente", false, true, false, false);
		setBounds(200, 100, 350, 230);
		init();
	}
	
	public ClienteDialog(Cliente cliente) {
		super("Editar Cliente", false, true, false, false);
		setBounds(200, 100, 350, 230);
		this.cliente=cliente;
		init();
	}
	
	private void init(){
		container = getContentPane();
		container.setBackground(SystemColor.scrollbar);
		container.setLayout(null);
		
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(8, 8, 325, 140);
		panel.setBorder(new EtchedBorder());
		
		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(font);
		lblNombre.setBounds(5, 8, 80, 28);
		lblNombre.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombre.setForeground(Color.DARK_GRAY);
		
		lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setFont(font);
		lblApellidos.setBounds(5, 38, 80, 28);
		lblApellidos.setHorizontalAlignment(SwingConstants.RIGHT);
		lblApellidos.setForeground(Color.DARK_GRAY);
		
		lblTelefono = new JLabel("Telefono:");
		lblTelefono.setFont(font);
		lblTelefono.setBounds(5, 68, 80, 28);
		lblTelefono.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTelefono.setForeground(Color.DARK_GRAY);
		
		lblEmail = new JLabel("Email:");
		lblEmail.setFont(font);
		lblEmail.setBounds(5, 98, 80, 28);
		lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmail.setForeground(Color.DARK_GRAY);
		
		nombre = new JTextField();
		nombre.setFont(font);
		nombre.setBounds(100, 8, 200, 28);
		
		apellidos = new JTextField();
		apellidos.setFont(font);
		apellidos.setBounds(100, 38, 200, 28);
		
		telefono = new JTextField();
		telefono.setFont(font);
		telefono.setBounds(100, 68, 170, 28);
		
		email = new JTextField();
		email.setFont(font);
		email.setBounds(100, 98, 170, 28);
		
		aceptar = new JButton("Guardar");
		aceptar.setBounds(25, 160, 100, 30);
		aceptar.addActionListener((e)->{
			persistCliente();
			closeFrame();
		});
		
		cancelar = new JButton("Cancelar");
		cancelar.setBounds(215, 160, 100, 30);
		cancelar.addActionListener(e->{
			closeFrame();
		});
		
		panel.add(lblNombre);
		panel.add(nombre);
		panel.add(lblApellidos);
		panel.add(apellidos);
		panel.add(lblTelefono);
		panel.add(telefono);
		panel.add(lblEmail);
		panel.add(email);
		
		container.add(aceptar);
		container.add(cancelar);
		container.add(panel);
	}
	
	public abstract void persistCliente();
	
	private void closeFrame(){
		this.dispose();
	}
}
