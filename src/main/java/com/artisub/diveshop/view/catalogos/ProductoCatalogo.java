package com.artisub.diveshop.view.catalogos;

import com.artisub.diveshop.controller.dao.IDAO;
import com.artisub.diveshop.controller.dao.ProductoDao;
import com.artisub.diveshop.model.Producto;
import com.artisub.diveshop.view.MainFrame;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

public class ProductoCatalogo extends Catalogo{

	private static final long serialVersionUID = -427568941200794323L;
	
	final double TIPO_DE_CAMBIO=com.artisub.diveshop.App.TIPO_DE_CAMBIO;
	
	private DecimalFormat decimalFormat = new DecimalFormat("#,###,###,##0.00");
	
	JLabel buscarLbl;
	JTextField searchText;
	JButton findBtn;
	
	final TableRowSorter<TableModel> sorter;
	
	public DefaultTableModel modelo = new DefaultTableModel();
	
	public IDAO<Producto> productoService = new ProductoDao();
    public List<Producto> lista_productos = productoService.findAll();
    
    protected static HashMap<Integer, Integer> posindex = new HashMap<>();

	public ProductoCatalogo() {
		super("Catalogo Productos", "Catalogo Productos");
		
		modelo.addColumn("Id");
		modelo.addColumn("# Parte");
		modelo.addColumn("Marca");
		modelo.addColumn("Articulo");
		modelo.addColumn("Modelo");
		modelo.addColumn("Color");
		modelo.addColumn("Talla");
		modelo.addColumn("Existencias");
		modelo.addColumn("Precio USD");
		modelo.addColumn("Precio MXN");
		
		table.setModel(modelo);
		
		setTableData();
		
		setSize(990, 520);
		setLocation(185, 75);
		
		buscarLbl = new JLabel("Buscar: ");
		buscarLbl.setFont(new Font("Lao UI",Font.PLAIN,18));
		searchText = new JTextField(20);
		searchText.requestFocus();
		searchText.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e){
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					filterRows();
				}
			}
		});
		findBtn = new JButton();
		
		menuActionPanel.add(buscarLbl);
		menuActionPanel.add(searchText);
		menuActionPanel.add(findBtn);
		
		table.setPreferredScrollableViewportSize(new Dimension(900,320));
		
//		UIDefaults defaults = UIManager.getLookAndFeelDefaults();
//		if (defaults.get("Table.alternateRowColor") == null)
//		    defaults.put("Table.alternateRowColor", new Color(235, 235, 240));
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
	    centerRenderer.setHorizontalAlignment( JLabel.CENTER );
	    table.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(5).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(6).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(7).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(8).setCellRenderer( centerRenderer );
	    table.getColumnModel().getColumn(9).setCellRenderer( centerRenderer );
	    
//	    DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
//	    rightRenderer.setHorizontalAlignment( JLabel.RIGHT );
//	    table.getColumnModel().getColumn(8).setCellRenderer( rightRenderer);
		
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setMaxWidth(0);
//		table.getColumnModel().getColumn(7).setMinWidth(0);
//		table.getColumnModel().getColumn(7).setMaxWidth(0);
		
		table.getColumnModel().getColumn(4).setPreferredWidth(200);
		
		
	    sorter = new TableRowSorter<TableModel>(modelo);
	    table.setRowSorter(sorter);
	    
	    findBtn.setBackground(Color.WHITE);
	    findBtn.setIcon(new ImageIcon("src/main/resources/Icons/lupa.png"));
	    findBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				filterRows();
			}
		});
		findBtn.requestFocus();
	}
	
	private void filterRows(){
		String expr = searchText.getText().toUpperCase();
        sorter.setRowFilter(RowFilter.regexFilter(expr));
        sorter.setSortKeys(null);
        int i = 0;
		for(Producto p : lista_productos){
			ProductoCatalogo.posindex.put(p.getId(), i);
			i++;
		}
	}
	
	public int getMapIndex(int id){
		int i = 0;
		for(Producto p : lista_productos){
			ProductoCatalogo.posindex.put(p.getId(), i);
			i++;
		}
		return posindex.get(id);
	}
	
	
	@Override
	protected void addItem() {
		NewProductoDialogo pd = new NewProductoDialogo(this);
		pd.setVisible(true);
		MainFrame.desktopPane.add(pd,JDesktopPane.POPUP_LAYER);
		try {
			pd.setSelected(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
	}
	
	@Override
	protected void editItem() {
//		Producto prod = productoService.findById((int)(table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0)));
		EditProductoDialogo ep = new EditProductoDialogo(this);
		ep.setVisible(true);
		
		MainFrame.desktopPane.add(ep, JDesktopPane.POPUP_LAYER);
		try {
			ep.setSelected(true);
		} catch (PropertyVetoException e1) {
			e1.printStackTrace();
		}
		
	}



	@Override
	protected void deleteItem() {
//		Producto prod = productoService.findById((lista_productos.get(table.getSelectedRow()).getId()));
		Producto prod = productoService.findById((int)(table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), 0)));
		int res = JOptionPane.showConfirmDialog(this, "Esta seguro de borrar \""+prod.getArticulo()+" "+prod.getDescripcion()+"\" ?");
		if(JOptionPane.OK_OPTION == res){
			productoService.delete(prod);
			lista_productos.remove(prod);
			posindex.clear();
			modelo.removeRow(getMapIndex(prod.getId()));
			
			
		}
		table.repaint();
	}

	@Override
	protected void setTableData() {
		for(Producto p : lista_productos){
			modelo.insertRow(modelo.getRowCount(), new Object[] {p.getId(),
																 p.getNumparte(), 
																 p.getMarca().getNombre(), 
																 p.getArticulo().getNombre(), 
																 p.getDescripcion(), 
																 p.getColor().getNombre(), 
																 p.getTalla().getNombre(), 
																 p.getExistencias(), 
																 decimalFormat.format(p.getPrecio()),
																 decimalFormat.format(p.getPrecio()*TIPO_DE_CAMBIO)});
		}
		
	}
	


}
