package com.artisub.diveshop.controller.dao;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Pedido;
import com.artisub.diveshop.model.Venta;

public class PedidoDao implements IDAO<Pedido> {
	
	EntityManagerFactorySingleton es = EntityManagerFactorySingleton.getInstance();
	EntityManager em = es.emf.createEntityManager();

	@Override
	@SuppressWarnings("unchecked")
	public List<Pedido> findAll() {
		List<Pedido> pedidos = em.createNamedQuery("Pedido.findAll").getResultList();
		em.clear();
		return pedidos;
	}

	@Override
	public Pedido findById(int id) {
		return em.find(Pedido.class, id);
	}

	@Override
	public Pedido create(Pedido obj) {
		em.getTransaction().begin();
		em.persist(obj);
		em.getTransaction().commit();
		return obj;
	}

	@Override
	public void delete(Pedido obj) {
		em.getTransaction().begin();
		em.remove(obj);
		em.getTransaction().commit();
	}

	@Override
	public Pedido update(Pedido obj) {
		em.getTransaction().begin();
		em.merge(obj);
		em.getTransaction().commit();
		return obj;
	}

	@Override
	public Pedido getByQuery(String namedquery, Properties props) throws NoResultException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<Pedido> findPedidosByDate(Date date){
		
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		Query query = em.createNamedQuery("Pedido.findPedidosByDate");
		query.setParameter("date", date);
		return query.getResultList();
	}

	public List<Pedido> findByNumVenta(int numventa){
		List<Pedido> pedido = null;
		Query query = em.createNamedQuery("Pedido.findByNumVenta");
		query.setParameter("numventa", numventa);
		pedido = query.getResultList();
		em.clear();
		return pedido;
	}
}
