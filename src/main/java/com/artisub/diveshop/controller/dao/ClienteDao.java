package com.artisub.diveshop.controller.dao;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.artisub.diveshop.model.Cliente;
import com.artisub.diveshop.model.Venta;

public class ClienteDao implements IDAO<Cliente> {

	EntityManagerFactorySingleton es = EntityManagerFactorySingleton.getInstance();
	EntityManager em = es.emf.createEntityManager();
	
	public ClienteDao() {
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> findAll() {
		List<Cliente> clientes;
		Query query = em.createNamedQuery("Cliente.findAll");
		clientes = query.getResultList();
		em.clear();
		return clientes;
	}

	@Override
	public Cliente findById(int id) {
		return em.find(Cliente.class, id);
	}

	@Override
	public Cliente create(Cliente obj) {
		em.getTransaction().begin();
//		obj.getVentas();
		em.persist(obj);
		em.getTransaction().commit();
		return obj;
	}
	
	

	@Override
	public void delete(Cliente obj) {
		em.getTransaction().begin();
		em.remove(obj);
		em.getTransaction().commit();
		
	}

	@Override
	public Cliente update(Cliente obj) {
		em.getTransaction().begin();
		em.merge(obj);
		em.getTransaction().commit();
		return obj;
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<Cliente> findClientePedido(String namedquery){
		Date date = new Date();
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		Query query = em.createNamedQuery("Cliente.findClientePedido");
		query.setParameter("date", date);
		return query.getResultList();
	}

	@Override
	public Cliente getByQuery(String namedquery, Properties props)
			throws NoResultException {
		Cliente cliente = null;
		Query q = null;
		if(!props.isEmpty()&&props!=null){
			Object[] keys = props.keySet().toArray();
			q = em.createNamedQuery(namedquery, Cliente.class).setMaxResults(1);
			q.setParameter((String) keys[0], props.getProperty((String) keys[0]));
			cliente = (Cliente) q.getSingleResult();
		}else{
			q = em.createNamedQuery(namedquery, Venta.class).setMaxResults(1);
			cliente = (Cliente) q.getSingleResult();
		}
		return cliente;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findAllCientesPedido(){
		List<Object[]> clientes;
		Query query = em.createNamedQuery("Cliente.findAllPedidos");
		clientes = query.getResultList();
		em.clear();
		return clientes;
	}
}
