package com.artisub.diveshop.controller.dao;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import com.artisub.diveshop.model.Marca;
import com.artisub.diveshop.model.Venta;

public class VentaDao implements IDAO<Venta> {

	EntityManagerFactorySingleton es = EntityManagerFactorySingleton.getInstance();
	EntityManager em = es.emf.createEntityManager();

	@SuppressWarnings("unchecked")
	@Override
	public List<Venta> findAll() {
		List<Venta> ventas;
		Query query = em.createNamedQuery("Venta.findAll");
		ventas = query.getResultList();
		em.clear();
		return ventas;
	}

	@Override
	public Venta findById(int id) {
		return em.find(Venta.class, id);
	}

	@Override
	public Venta create(Venta obj) {
		em.getTransaction().begin();
		em.persist(obj);
		em.getTransaction().commit();
		return obj;
	}

	@Override
	public void delete(Venta obj) {
		em.getTransaction().begin();
		em.remove(obj);
		em.getTransaction().commit();
	}

	@Override
	public Venta update(Venta obj) {
		em.getTransaction().begin();
		em.merge(obj);
		em.getTransaction().commit();
		return obj;
	}

	@Override
	public Venta getByQuery(String namedquery, Properties props)
			throws NoResultException {
		Venta venta = null;
		Query q = null;
		if(!props.isEmpty() ){
			Object[] keys = props.keySet().toArray();
			q = em.createNamedQuery(namedquery, Venta.class).setMaxResults(1);
			q.setParameter((String) keys[0], props.getProperty((String) keys[0]));
			venta = (Venta) q.getSingleResult();
		}else{
			q = em.createNamedQuery(namedquery, Venta.class).setMaxResults(1);
			venta = (Venta) q.getSingleResult();
		}
		return venta;
	}
	
	@SuppressWarnings("unchecked")
	public List<Venta> findSales(Date date1, Date date2){
//		date1.setHours(0);
//		date1.setMinutes(0);
//		date1.setSeconds(0);
//		date2.setHours(23);
//		date2.setMinutes(59);
//		date2.setSeconds(59);
		List<Venta> ventas = null;
		Query query = em.createNamedQuery("Venta.findSales");
		query.setParameter("date1", date1);
		query.setParameter("date2", date2);
		ventas = query.getResultList();
		em.clear();
		return ventas;
	}
	
	@SuppressWarnings("unchecked")
	public List<Venta> findPedidosTotales(Date date1, Date date2){
//		date1.setHours(0);
//		date1.setMinutes(0);
//		date1.setSeconds(0);
		List<Venta> ventas = null;
		Query query = em.createNamedQuery("Venta.findPedidosTotales");
		query.setParameter("date1", date1);
		query.setParameter("date2", date2);
		ventas = query.getResultList();
		em.clear();
		return ventas;
	}
	
	@SuppressWarnings("unchecked")
	public List<Venta> findVentasMes(Date date1, Date date2){
		List<Venta> ventas = null;
		Query query = em.createNamedQuery("Venta.findVentasMes");
		query.setParameter("date1", date1);
		query.setParameter("date2", date2);
		ventas = query.getResultList();
		em.clear();
		return ventas;
	}
	@SuppressWarnings("unchecked")
	public List<Venta> findAllPedidos(){
		List<Venta> ventasSobrePedido = null;
		Query query = em.createNamedQuery("Venta.findAllPedidos");
		ventasSobrePedido = query.getResultList();
		em.clear();
		return ventasSobrePedido;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findClientePedido(int numventa){
		List<Object[]> pedidos = null;
		Query query = em.createNamedQuery("Venta.findClientePedido");
		query.setParameter("numventa", numventa);
		pedidos = query.getResultList();
		em.clear();
		return pedidos;
	}
	
	 /**
	  * 
	  * @param numventa
	  */
	@SuppressWarnings("unchecked")
	public List<Venta> cancelVenta(int numventa){
		List<Venta> ventas = null;
		Query query = em.createNamedQuery("Venta.findByNumVenta");
		query.setParameter("numventa", numventa);
		ventas = query.getResultList();
		for(Venta v : ventas){
			v.setCancel(true);
			update(v);
		}
		em.clear();
		return ventas;
	}
	
	@SuppressWarnings("unchecked")
	public List<Venta> findByNumVenta(int numventa){
		List<Venta> articlos_vendidos = null;
		Query query = em.createNamedQuery("Venta.findByNumVenta");
		query.setParameter("numventa", numventa);
		articlos_vendidos = query.getResultList();
		em.clear();
		return articlos_vendidos;
	}
	
	@SuppressWarnings("unchecked")
	public List<Venta> findByMarca(Marca marca){
		Query query = em.createNamedQuery("Venta.findByMarca"); 
		query.setParameter("marca", marca);
		em.clear();
		return query.getResultList();
	}
}
