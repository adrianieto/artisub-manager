package com.artisub.diveshop;

import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.artisub.diveshop.view.MainFrame;

public class App{
	
	public static final double TIPO_DE_CAMBIO = 22;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				        if ("Metal".equals(info.getName())) {
				            UIManager.setLookAndFeel(info.getClassName());
				            UIManager.getLookAndFeelDefaults().put("ScrollBar.minimumThumbSize", new Dimension(30, 30));
				            break;
				        }
				    }
					
					MainFrame mf = new MainFrame();
//					mf.add(new ImageIcon("src/main/resources/wallpaper-artisub.jpg"));
//					mf.setContentPane(new JLabel(new ImageIcon(ImageIO.read(new File("src/main/resources/wallpaper-artisub.jpg")))));

					
					mf.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}