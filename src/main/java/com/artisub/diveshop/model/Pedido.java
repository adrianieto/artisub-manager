package com.artisub.diveshop.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the pedidos database table.
 * 
 */
@Entity
@Table(name="pedidos")
@NamedQueries({
	@NamedQuery(name="Pedido.findAll", query="SELECT p FROM Pedido p"),
	@NamedQuery(name="Pedido.findPedidosByDate", query="Select p from Pedido p WHERE p.fecha BETWEEN :date AND CURRENT_TIMESTAMP"),
	@NamedQuery(name="Pedido.findByNumVenta", query="SELECT p FROM Pedido p WHERE p.numventa = :numventa")
	
})

public class Pedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idpedidos;

	private Double anticipo;

	private boolean pedidoEntregado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	private Double resto;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha2;
	
	@ManyToOne
	@JoinColumn(name="clientes_id")
	private Cliente cliente;

	//bi-directional many-to-one association to Formapago
	@ManyToOne
	private Formapago formapago;

	private int numventa;

	public Pedido() {
	}

	public int getIdpedidos() {
		return this.idpedidos;
	}

	public void setIdpedidos(int idpedidos) {
		this.idpedidos = idpedidos;
	}

	public Double getAnticipo() {
		return this.anticipo;
	}

	public void setAnticipo(Double anticipo) {
		this.anticipo = anticipo;
	}

	public boolean getPedidoEntregado() {
		return this.pedidoEntregado;
	}

	public void setPedidoEntregado(boolean pedidoEntregado) {
		this.pedidoEntregado = pedidoEntregado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Formapago getFormapago() {
		return this.formapago;
	}

	public void setFormapago(Formapago formapago) {
		this.formapago = formapago;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getNumVenta() {
		return numventa;
	}

	public void setNumVenta(int numventa) {
		this.numventa = numventa;
	}

	public Double getResto() {
		return resto;
	}

	public void setResto(Double resto) {
		this.resto = resto;
	}

	public Date getFecha2() {
		return fecha2;
	}

	public void setFecha2(Date fecha2) {
		this.fecha2 = fecha2;
	}

	public int getNumventa() {
		return numventa;
	}

	public void setNumventa(int numventa) {
		this.numventa = numventa;
	}
	
	

}