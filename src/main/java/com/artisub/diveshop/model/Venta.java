package com.artisub.diveshop.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the ventas database table.
 * 
 */
@Entity
@Table(name="ventas")
@NamedQueries({
	@NamedQuery(name="Venta.findAll", query="SELECT v FROM Venta v"),
	@NamedQuery(name="Venta.findAllPedidos", query="SELECT v FROM Venta v WHERE v.pedido = TRUE"),
	@NamedQuery(name="Venta.findLast", query="SELECT v FROM Venta v ORDER BY v.id DESC"),
	@NamedQuery(name="Venta.findSales",query="SELECT v FROM Venta v WHERE v.feha BETWEEN :date1 AND :date2 AND v.pedido = FALSE AND v.cancel = FALSE"),
//	@NamedQuery(name="Venta.findClientePedido", query="SELECT v,c FROM Venta v JOIN v.clientes c WHERE v.numventa = :numventa"),
	@NamedQuery(name="Venta.findPedidosTotales",query="SELECT v FROM Venta v WHERE v.feha BETWEEN :date1 AND :date2 AND v.pedido = TRUE"),
	@NamedQuery(name="Venta.findVentasMes",query="SELECT v FROM Venta v WHERE v.feha BETWEEN :date1 AND :date2 AND v.pedido = FALSE AND v.cancel = FALSE"),
	@NamedQuery(name="Venta.findByNumVenta",query="SELECT v FROM Venta v WHERE v.numventa = :numventa"),
	@NamedQuery(name="Venta.findByMarca", query="SELECT v FROM Venta v WHERE v.producto.marca = :marca")
})
public class Venta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private Short cantidad;

	private String comentarios;

	private Double descuento;

	@Temporal(TemporalType.TIMESTAMP)
	private Date feha;

	private int numventa;

	private Double subtotal;

	private Double total;
	
	private boolean pedido;
	
	private boolean cancel;

	// bi-directional many-to-one association to Formapago
	@ManyToOne
	private Formapago formapago;

	// bi-directional many-to-one association to Producto
	@ManyToOne
	private Producto producto;

	public Venta() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Short getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Short cantidad) {
		this.cantidad = cantidad;
	}

	public String getComentarios() {
		return this.comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public Double getDescuento() {
		return this.descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public Date getFeha() {
		return this.feha;
	}

	public void setFeha(Date feha) {
		this.feha = feha;
	}

	public int getNumventa() {
		return this.numventa;
	}

	public void setNumventa(int numventa) {
		this.numventa = numventa;
	}

	public Double getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Formapago getFormapago() {
		return this.formapago;
	}

	public void setFormapago(Formapago formapago) {
		this.formapago = formapago;
	}

	public Producto getProducto() {
		return this.producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public boolean isPedido() {
		return pedido;
	}

	public void setPedido(boolean pedido) {
		this.pedido = pedido;
	}

	public boolean isCancel() {
		return cancel;
	}

	public void setCancel(boolean cancel) {
		this.cancel = cancel;
	}
	
	

}