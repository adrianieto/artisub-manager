package com.artisub.diveshop.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the producto database table.
 * 
 */
@Entity
@NamedQueries({@NamedQuery(name="Producto.findAll", query="SELECT p FROM Producto p WHERE p.prod_no_listado=false"),
	   @NamedQuery(name="Producto.findNumParte", query="SELECT p FROM Producto p WHERE p.numparte=:numparte"),
	   @NamedQuery(name="Producto.findCodigo", query="SELECT c FROM Producto c WHERE c.codigo=:codigo")
	   })
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String codigo;

	private String descripcion;

	private Integer existencias;

	private String numparte;

	private Double precio;
	
	private Boolean prod_no_listado;

	//bi-directional many-to-one association to Articulo
	@ManyToOne
	private Articulo articulo;

	//bi-directional many-to-one association to Color
	@ManyToOne
	private Colors color;

	//bi-directional many-to-one association to Marca
	@ManyToOne
	private Marca marca;

	//bi-directional many-to-one association to Talla
	@ManyToOne
	private Talla talla;

	//bi-directional many-to-one association to Venta
	@OneToMany(mappedBy="producto")
	private List<Venta> ventas;

	public Producto() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getExistencias() {
		return this.existencias;
	}

	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

	public String getNumparte() {
		return this.numparte;
	}

	public void setNumparte(String numparte) {
		this.numparte = numparte;
	}

	public Double getPrecio() {
		return this.precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Articulo getArticulo() {
		return this.articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

	public Colors getColor() {
		return this.color;
	}

	public void setColor(Colors color) {
		this.color = color;
	}

	public Marca getMarca() {
		return this.marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public Talla getTalla() {
		return this.talla;
	}

	public void setTalla(Talla talla) {
		this.talla = talla;
	}

	public List<Venta> getVentas() {
		return this.ventas;
	}

	public void setVentas(List<Venta> ventas) {
		this.ventas = ventas;
	}

	public Venta addVenta(Venta venta) {
		getVentas().add(venta);
		venta.setProducto(this);

		return venta;
	}

	public Venta removeVenta(Venta venta) {
		getVentas().remove(venta);
		venta.setProducto(null);

		return venta;
	}

	public Boolean getProd_no_listado() {
		return prod_no_listado;
	}

	public void setProd_no_listado(Boolean prod_no_listado) {
		this.prod_no_listado = prod_no_listado;
	}

	
}